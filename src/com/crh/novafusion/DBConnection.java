package com.crh.novafusion;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection extends SQLiteOpenHelper{
//	public static final String DATABASE_NAME = "Novafusion.db";
	public static final String DATABASE_NAME = Global.APP_DIR + "Novafusion.db";
	public static final int DATABASE_VERSION = 1;
	
	public static final String PRODUCT_TABLE = "Product";
	public static final String BOOMARK_TABLE = "Bookmark";
	public static final String MYJOB_TABLE = "Myjob";
	
	public DBConnection(Context context, CursorFactory factory) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sqll = "CREATE TABLE IF NOT EXISTS `Myjob` ("
				+ "`MyjobID` INTEGER PRIMARY KEY,"
				+ "`Title` NUMERIC DEFAULT NULL,"
				+ "`Date` Text DEFAULT CURRENT_DATE"
			    + ")";
		db.execSQL(sqll);
		
		sqll = "CREATE TABLE IF NOT EXISTS `Bookmark` ("
				+ "`BookmarkID` INTEGER PRIMARY KEY,"
				+ "`Title` TEXT DEFAULT NULL,"
				+ "`ItemID` TEXT DEFAULT NULL,"
				+ "`Code` TEXT DEFAULT NULL,"
				+ "`Trade` int(100) DEFAULT '0',"
				+ "`Retail` TEXT DEFAULT '1',"
				+ "`Description` TEXT DEFAULT '',"
				+ "`ImagePathS` TEXT DEFAULT '',"
				+ "`ImagePathM` TEXT DEFAULT '',"
				+ "`ImageGallery` TEXT DEFAULT '',"
				+ "`ImageTechnical` TEXT DEFAULT '',"
				+ " `SectionID` int(100) DEFAULT '0',"
				+ " `CategoryID` int(100) DEFAULT '0'"
				+ ")";
		db.execSQL(sqll);
		
		sqll = "CREATE TABLE IF NOT EXISTS `Product` ("
				+ "`ProductID` INTEGER PRIMARY KEY,"
				+ "`Title` TEXT DEFAULT NULL,"
				+ "`ItemID` TEXT DEFAULT NULL,"
				+ "`Code` TEXT DEFAULT NULL,"
				+ "`Trade` int(100) DEFAULT '0',"
				+ "`Quantity` int(100) DEFAULT '0',"
				+ "`Jobid` int(100) DEFAULT '0',"
				+ "`Retail` TEXT DEFAULT '0',"
				+ "`Description` TEXT DEFAULT '',"
				+ "`ImagePathS` TEXT DEFAULT '',"
				+ "`ImagePathM` TEXT DEFAULT '',"
				+ "`ImageGallery` TEXT DEFAULT '',"
				+ "`ImageTechnical` TEXT DEFAULT '',"
				+ " `SectionID` int(100) DEFAULT '0',"
				+ " `CategoryID` int(100) DEFAULT '0'"
				+ ")";
		db.execSQL(sqll);
}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	
}
