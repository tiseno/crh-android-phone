package com.crh.novafusion;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import uk.co.senab.photoview.PhotoView;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crh.novafusion.imageloader.ImageLoader;

public class Global {
	static final String APP_DIR = Environment.getExternalStorageDirectory()
			+ File.separator + "/Novafusionss/";

	static final int QUANTITY = 100;

	static final int ADD_JOB = 101;

	private Context context;

	static DBConnection helper;
	static SQLiteDatabase db;
	static DatabaseAssistance da;

	static InputMethodManager imm;

	public Global(Context context) {

		if (helper == null || db == null || da == null) {
			helper = new DBConnection(context, null);
			db = helper.getWritableDatabase();
			da = new DatabaseAssistance(db);
		}
		this.context = context;

		imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
	}

	public void setTopbarColor(LinearLayout ll, int colorIndex) {
		int[] tbc = context.getResources().getIntArray(R.array.top_bar_color1);
		int[] tbc2 = context.getResources().getIntArray(R.array.top_bar_color2);

		GradientDrawable gd = new GradientDrawable(
				GradientDrawable.Orientation.TOP_BOTTOM, new int[] {
						tbc[colorIndex], tbc2[colorIndex] });
		ll.setBackgroundDrawable(gd);
	}

	// date format -> 2011-2-14 bian cheng 2011 feb 14
	public String genarateDateString(String sdate) {
		// format year-mont-day
		String[] date = sdate.split("-");
		int year = Integer.parseInt(date[0]);
		int month = Integer.parseInt(date[1]);
		int day = Integer.parseInt(date[2]);
		Calendar c = Calendar.getInstance();
		c.set(year, month, day);
		return String.format("%tA %tY %tB %td", c, c, c, c);

	}

	public void hideKeyboard(View v) {
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	public void showKeyboard() {
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	// image gallery dialog
	public void popImageGallery(String[] urls, String title) {
		// custom dialog
		final Dialog dialog = new Dialog(context,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.dialog_image_gallery);
		dialog.setCancelable(true);

		final ViewPager mViewPager = (ViewPager) dialog
				.findViewById(R.id.dialog_image_gallery_vPager);
		final SamplePagerAdapter adapter = new SamplePagerAdapter(context, urls);
		mViewPager.setAdapter(adapter);

		TextView tvTitle = (TextView) dialog
				.findViewById(R.id.dialog_image_gallery_tv_title);
		tvTitle.setText(title + "");

		Button btClose = (Button) dialog
				.findViewById(R.id.dialog_image_gallery_bt_close);
		btClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

	}

	static class SamplePagerAdapter extends PagerAdapter {
		private ImageLoader imageLoader;
		private String[] urls;

		public SamplePagerAdapter(Context context, String[] urls) {
			imageLoader = new ImageLoader(context);
			this.urls = urls;
		}

		@Override
		public int getCount() {
			return urls.length;
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			PhotoView photoView = new PhotoView(container.getContext());
			imageLoader.setImageSize(512);
			imageLoader.DisplayImage(urls[position], photoView);
			
			// photoView.setImageResource(sDrawables[position]);

			// Now just add PhotoView to ViewPager and return it
			container.addView(photoView, LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);

			return photoView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}

	// email dialog
	public void popEmailDialog(final Context context, final Bundle bundle) 
	{
		// custom dialog
		Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.email_dialog);
		dialog.setCancelable(true);

		final Button btnCompleteDetails = (Button) dialog.findViewById(R.id.dialog_popup_bt_complete);
		final Button btnWithoutPrice = (Button) dialog.findViewById(R.id.dialog_popup_bt_without_price);
		final Button btnSendPlainText = (Button) dialog.findViewById(R.id.dialog_popup_bt_text);
		final Button btnSendhtml = (Button) dialog.findViewById(R.id.dialog_popup_bt_html);
		final Button btnSendpdf = (Button) dialog.findViewById(R.id.dialog_popup_bt_pdf);
		
		StringBuffer action = new StringBuffer();
		
		dialog.setOnKeyListener(new OnKeyListener() 
		{
			@Override
			public boolean onKey(DialogInterface arg0, int arg1, KeyEvent event) 
			{
				if (event.getAction() == KeyEvent.ACTION_UP) 
				{
					if ((arg1 == KeyEvent.KEYCODE_BACK)) 
					{
						if (btnCompleteDetails.getVisibility() == View.VISIBLE) 
						{
							
						} 
						else 
						{
							btnCompleteDetails.setVisibility(View.VISIBLE);
							btnWithoutPrice.setVisibility(View.VISIBLE);

							btnSendPlainText.setVisibility(View.GONE);
							btnSendhtml.setVisibility(View.GONE);
							btnSendpdf.setVisibility(View.GONE);

							return true;
						}
					}
				}
					return false;
			}
		});
		
		//with price
		btnCompleteDetails.setTag(action);
		btnCompleteDetails.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				btnCompleteDetails.setVisibility(View.GONE);
				btnWithoutPrice.setVisibility(View.GONE);

				btnSendPlainText.setVisibility(View.VISIBLE);
				btnSendhtml.setVisibility(View.VISIBLE);
				btnSendpdf.setVisibility(View.VISIBLE);
				
				StringBuffer set = (StringBuffer)v.getTag();
				set.delete(0,  set.length());
				set.append("withPrice");
			}
		});
		
		//without price
		btnWithoutPrice.setTag(action);
		btnWithoutPrice.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				btnCompleteDetails.setVisibility(View.GONE);
				btnWithoutPrice.setVisibility(View.GONE);

				btnSendPlainText.setVisibility(View.VISIBLE);
				btnSendhtml.setVisibility(View.VISIBLE);
				btnSendpdf.setVisibility(View.VISIBLE);
				
				StringBuffer set = (StringBuffer)v.getTag();
				set.delete(0,  set.length());
				set.append("withoutPrice");
			}
		});

		btnSendPlainText.setTag(action);
		btnSendPlainText.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				StringBuffer str = (StringBuffer)v.getTag();
				
				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.setType("text/html");
				
				String type = bundle.getString("type");
				
				// check if from bookmarks or products
				if(type.equals("bookmarks") || type.equals("products"))
				{
					ArrayList<Product> data = new ArrayList<Product>();
					
					if(type.equals("bookmarks"))
					{
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH - My Bookmarks");
						
						data = bundle.getParcelableArrayList("bookmarks");
					}
					else
					{
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH - My Jobs");
						
						data = bundle.getParcelableArrayList("products");
					}
						
					StringBuilder sb = new StringBuilder();
					
					//check with or without price 
					if(str.toString().equals("withPrice"))
					{	
						for(int i = 0; i < data.size(); i++)
						{
							Product product = new Product();
							product = data.get(i);
							
							sb.append((i+1) + ") Title: " + product.Title + "<br /><br />"
									+ "Code : " + product.Code + "<br /><br />"
									+ "Trade : " + product.Trade + "<br /><br />"
									+ "Retail : " + product.Retail + "<br /><br />"
									+ product.Description + "<br /><br />");
						}
					}
					else
					{
						for(int i = 0; i < data.size(); i++)
						{
							Product product = new Product();
							product = data.get(i);
							
							sb.append((i+1) + ") Title: " + product.Title + "<br /><br />"
									+ "Code : " + product.Code + "<br /><br />"
									+ product.Description + "<br /><br />");
						}
					}
					
					emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(sb.toString()));
				}
				else
				{
					emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH Product Details");
					
					//check with or without price 
					if(str.toString().equals("withPrice"))
					{					
						emailIntent.putExtra(Intent.EXTRA_TEXT, 
								Html.fromHtml(new StringBuilder().append(bundle.getString("hardwareTitle") + "<br /><br />"
										+ "Code : " + bundle.getString("hardwareCode") + "<br /><br />"
										+ "Trade :" + bundle.getString("hardwareTrade") + "<br /><br />"
										+ "Retail : " + bundle.getString("hardwareRetail") + "<br /><br />"
										+ bundle.getString("hardwareDesc")).toString()));
					}
					else
					{					
						emailIntent.putExtra(Intent.EXTRA_TEXT, 
								Html.fromHtml(new StringBuilder().append(bundle.getString("hardwareTitle") + "<br /><br />"
										+ "Code : " + bundle.getString("hardwareCode") + "<br /><br />"
										+ bundle.getString("hardwareDesc")).toString()));
					}
				}
				
				context.startActivity(emailIntent);
			}
		});
		
		btnSendhtml.setTag(action);
		btnSendhtml.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				StringBuffer str = (StringBuffer)v.getTag();
				
				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.setType("text/html");
				
				String type = bundle.getString("type");
				
				// check if from bookmarks or products
				if(type.equals("bookmarks") || type.equals("products"))
				{
					ArrayList<Product> data = new ArrayList<Product>();
					
					if(type.equals("bookmarks"))
					{
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH - My Bookmarks");
						
						data = bundle.getParcelableArrayList("bookmarks");
					}
					else
					{
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH - My Jobs");
						
						data = bundle.getParcelableArrayList("products");
					}

					StringBuilder sb = new StringBuilder();
					
					//check with or without price 
					if(str.toString().equals("withPrice"))
					{
						for(int i = 0; i < data.size(); i++)
						{
							Product product = new Product();
							product = data.get(i);
							
							sb.append("<img src=\"" + product.ImagePathM + "\" /><br /><br />"
									+ (i+1) + ") Title : " + product.Title + "<br /><br />"
									+ "Code : " + product.Code + "<br /><br />"
									+ "Trade : " + product.Trade + "<br /><br />"
									+ "Retail : " + product.Retail + "<br /><br />"
									+ product.Description + "<br /><br />");
						}
					}
					else
					{
						for(int i = 0; i < data.size(); i++)
						{
							Product product = new Product();
							product = data.get(i);
						
							sb.append("<img src=\"" + product.ImagePathM + "\" /><br /><br />"
									+ (i+1) + ") Title : " + product.Title + "<br /><br />"
									+ "Code : " + product.Code + "<br /><br />"
									+ product.Description + "<br /><br />");
						}
					}
					
					emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(sb.toString()));
				}
				else
				{
					emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH Product Details");
					
					//check with or without price 
					if(str.toString().equals("withPrice"))
					{	
						emailIntent.putExtra(
								Intent.EXTRA_TEXT,
								Html.fromHtml(new StringBuilder().append(
										"<img src=\"" + bundle.getString("hardwareImagePath") + "\" /><br /><br />"
										+ bundle.getString("hardwareTitle") + "<br /><br />"
										+ "Code : " + bundle.getString("hardwareCode") + "<br /><br />"
										+ "Trade :" + bundle.getString("hardwareTrade") + "<br /><br />"
										+ "Retail : " + bundle.getString("hardwareRetail") + "<br /><br />"
										+ bundle.getString("hardwareDesc")).toString()));
					}
					else
					{
						emailIntent.putExtra(
								Intent.EXTRA_TEXT,
								Html.fromHtml(new StringBuilder().append(
										"<img src=\"" + bundle.getString("hardwareImagePath") + "\" /><br /><br />"
										+ bundle.getString("hardwareTitle") + "<br /><br />"
										+ "Code : " + bundle.getString("hardwareCode") + "<br /><br />"
										+ bundle.getString("hardwareDesc")).toString()));
					}
				}
				
				context.startActivity(emailIntent);
			}
		});

		
		//send details to server for pdf generation
		btnSendpdf.setTag(action);
		btnSendpdf.setOnClickListener(new OnClickListener() 
		{
			@SuppressLint("SimpleDateFormat")
			@Override
			public void onClick(View v) 
			{
				StringBuffer str = (StringBuffer)v.getTag();
				
				Long tsLong = System.currentTimeMillis()/1000;
				String TimeStamp = tsLong.toString();
				
				String type = bundle.getString("type");
				int time = 1;
				
				WebServices ws = new WebServices();
				
				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.setType("text/html");
				
				// check if from bookmarks or products
				if(type.equals("bookmarks") || type.equals("products"))
				{
					ArrayList<Product> data = new ArrayList<Product>();
					
					if(type.equals("bookmarks"))
					{
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH - My Bookmarks");
						
						data = bundle.getParcelableArrayList("bookmarks");
					}
					else
					{
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH - My Jobs");
						
						data = bundle.getParcelableArrayList("products");
					}
					
					StringBuilder sb = new StringBuilder();
					
					sb.append("[");
					
					//check with or without price 
					if(str.toString().equals("withPrice"))
					{
						for(int i = 0; i < data.size(); i++)
						{
							Product product = new Product();
							product = data.get(i);
							
							String imagePath = product.ImagePathM
									.replace("http://crh-app.novafusion.net:8052", "..");
							
							sb.append("{\"Title\":\"" + product.Title
									+ "\",\"Code\":\"" + product.Code
									+ "\",\"Trade\":\"" + product.Trade
									+ "\",\"Retail\":\"" + product.Retail
									+ "\",\"Description\":\"" + product.Description
									+ "\",\"ImagePath\":\"" + imagePath
									+ "\",\"TimeStamp\":\"" + TimeStamp
									+ "\",\"Quantity\":\"" + product.Quantity
									+ "\",\"TotalTrade\":\"" + bundle.getString("totalTrade")
									+ "\",\"TotalRetail\":\"" + bundle.getString("totalRetail") + "\"}");
							
							if((i+1) != data.size())
							{
								sb.append(",");
							}
						}
						
						sb.append("]");
									
						ws.generateListOfItemsPDFWithPrice(sb.toString());
					}
					else
					{
						for(int i = 0; i < data.size(); i++)
						{
							Product product = new Product();
							product = data.get(i);
						
							String imagePath = product.ImagePathM
									.replace("http://crh-app.novafusion.net:8052", "..");
							
							sb.append("{\"Title\":\"" + product.Title
									+ "\",\"Code\":\"" + product.Code
									+ "\",\"Description\":\"" + product.Description
									+ "\",\"ImagePath\":\"" + imagePath
									+ "\",\"TimeStamp\":\"" + TimeStamp
									+ "\",\"Quantity\":\"" + product.Quantity + "\"}");
							
							if((i+1) != data.size())
							{
								sb.append(",");
							}
						}
						
						sb.append("]");
						
						ws.generateListOfItemsPDFWithoutPrice(sb.toString());
					}
				}
				else
				{
					String imagePath = bundle.getString("hardwareImagePath")
							.replace("http://crh-app.novafusion.net:8052", "..");
					
					//check with or without price
					if(str.toString().equals("withPrice"))
					{	
			            ws.generatePDF_FileWithPrice(bundle.getString("hardwareTitle"), 
			            		bundle.getString("hardwareCode"), bundle.getString("hardwareTrade"), 
			            		bundle.getString("hardwareRetail"), bundle.getString("hardwareDesc"), 
			            		imagePath, TimeStamp);
					}
					else
					{
			            ws.generatePDF_FileWithoutPrice(bundle.getString("hardwareTitle"), 
			            		bundle.getString("hardwareCode"), bundle.getString("hardwareDesc"), 
			            		imagePath, TimeStamp);
					}
				}
				
				//check pdf generation status
				if(ws.pdfStatus.equals("Success"))
				{
					emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "CRH Product Details");
					
					String link_val = "http://crh-app.novafusion.net:8052/tiseno/fpdf17/product_pdf/"
							+ TimeStamp + ".pdf";

					emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(new StringBuilder().append(
							"Click here to download the pdf file : <a href=\"" + link_val + "\">" 
									+ link_val + "</a>").toString()));

					context.startActivity(emailIntent);
				}
				else
				{
					Toast.makeText(context, "fail to generate pdf", time).show();
				}
			}
		});

		dialog.show();
	}
    
    public String filterSpacialChar(String str) {
		str = str.replace("ºC", "celciusSign");
		str = str.replace("ºF", "fahrenheitSign");
		str = str.replace("º", "degreeSign");
		str = str.replace("®", "(registered trademark)");
		str = str.replace("µm", "micrometer");
		str = str.replace("¼", " 1/4");
		str = str.replace("½", " 1/2");
		str = str.replace("©", "(copyrighted)");
		str = str.replace("\n", "<br />");
		str = str.replace("\"", "12345_");
        
		Log.i("replaceSpeacialChar", ""+str);
		return str;
	}

}