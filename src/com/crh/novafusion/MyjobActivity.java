package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MyjobActivity extends Activity implements OnClickListener,
		OnItemClickListener, OnItemLongClickListener {
	private ListView listview;
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
	private Global global;

	private Button btAddjob;

	String[] from = { "Title", "Date" };
	int[] to = { R.id.cell_job2_tv_title, R.id.cell_job2_tv_date };

	ProgressBar dialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myjob);
		global = new Global(this);

		initView();
		Toast.makeText(this, "Long press to delete job.", 1500).show();

	}

	private void initView() {
		listview = (ListView) findViewById(R.id.myjob_lv_listview);
		listview.setOnItemClickListener(this);
		listview.setOnItemLongClickListener(this);
		btAddjob = (Button) findViewById(R.id.myjob_bt_addjob);
		btAddjob.setOnClickListener(this);

		proccessListData();
	}

	private void proccessListData() {
		Cursor c = global.da.getAllJob();
		data.clear();
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			HashMap<String, Object> _data = new HashMap<String, Object>();
			_data.put("Date", global.genarateDateString(c.getString(c
					.getColumnIndex("Date"))));
			_data.put("Title", c.getString(c.getColumnIndex("Title")));
			_data.put("MyjobID", c.getInt(c.getColumnIndex("MyjobID")));
			data.add(_data);
		}

		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.cell_job2, from, to);
		listview.setAdapter(adapter);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.myjob_bt_addjob:
			popDialog();
			break;
		}
	}

	private void popDialog() {
		// custom dialog
		final Dialog dialog = new Dialog(this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.dialog_popup);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);

		TextView text = (TextView) dialog
				.findViewById(R.id.dialog_popup_tv_title);
		text.setText("Job Title");

		final EditText field = (EditText) dialog
				.findViewById(R.id.dialog_popup_ed_textField);

		Button btAdd = (Button) dialog.findViewById(R.id.dialog_popup_bt_Add);
		btAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String title = field.getText().toString();
				if (!title.equals("")) {
					addJob(title);
					dialog.dismiss();
					global.hideKeyboard(v);
				} else {
					Toast.makeText(MyjobActivity.this,
							"Tittle cannot empty !! ", 1000).show();
				}
			}
		});

		Button btCancel = (Button) dialog
				.findViewById(R.id.dialog_popup_bt_cancel);
		btCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				field.clearFocus();
				global.hideKeyboard(v);
				dialog.dismiss();
			}
		});

		dialog.show();
		global.showKeyboard();
	}

	private void popDeleteRowDialog(final int position) {
		final Dialog dialog = new Dialog(this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.dialog_popup_question);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);

		TextView text = (TextView) dialog
				.findViewById(R.id.dialog_popup_tv_title);

		text.setText("Sure want to delete job "
				+ data.get(position).get("Title") + " ?");

		Button btAdd = (Button) dialog.findViewById(R.id.dialog_popup_bt_Add);
		btAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				global.da.DeleteMyjob((Integer) data.get(position).get(
						"MyjobID"));
				proccessListData();
				dialog.dismiss();
			}
		});

		Button btCancel = (Button) dialog
				.findViewById(R.id.dialog_popup_bt_cancel);
		btCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void addJob(String title) {
		ContentValues cv = new ContentValues();
		cv.put("Title", "" + title);
		long id = global.da.insertMyjob(cv);
		proccessListData();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		Intent intent = new Intent(MyjobActivity.this, JobInfoActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("MyjobID", (Integer) data.get(position).get("MyjobID"));
		bundle.putString("Title", (String) data.get(position).get("Title"));
		intent.putExtras(bundle);
		startActivity(intent);
		overridePendingTransition(R.anim.right_enter,
				R.anim.right_exit);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
			int position, long arg3) {
		popDeleteRowDialog(position);
		return false;
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.no_anim,
				R.anim.slide_out_down);
	}
}
