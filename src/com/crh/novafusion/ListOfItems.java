package com.crh.novafusion;

import java.util.HashMap;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ListOfItems extends Activity implements OnItemClickListener,
		AsyncTaskCompleteListener<Object> {
	private WebServices ws;
	private Bundle bundle;
	private String catID, colorIndex, catName;
	private ListView list;
	private ListOfItemsAdapter adapter;
	private TextView txtName;
	private LinearLayout ll;
	private ProgressBar progressBar;

	static final String KEY_NAME = "name", KEY_TYPE = "type",
			KEY_APPID = "application_id", KEY_ID = "id", KEY_DESC = "desc",
			KEY_PRICE = "price", KEY_IMGPATH1 = "imagePath1",
			KEY_IMGPATH2 = "imagePath2", KEY_INCGST = "incgst",
			KEY_CODE = "code", KEY_THUMBPATH = "thumbPath",
			KEY_MIDDLE_IMAGE = "middleImage", KEY_TRADE = "trade",
			KEY_TRADE_SUFFIX = "tradeSuffix", KEY_PRICE_SUFFIX = "priceSuffix";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.items_list);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		list = (ListView) findViewById(R.id.itemListView);

		try 
		{
			bundle = this.getIntent().getExtras();

			catID = bundle.getString("catID");
			colorIndex = bundle.getString("colorIndex");
			catName = bundle.getString("catName");

			txtName = (TextView) findViewById(R.id.item_list_cat_name);
			txtName.setText(catName);

			ll = (LinearLayout) findViewById(R.id.linearLayout1);
			
			Global global = new Global(this);
			global.setTopbarColor(ll, Integer.parseInt(colorIndex));

			progressBar = (ProgressBar) findViewById(R.id.progressBar);

			Class[] parameterTypes = new Class[1];
			parameterTypes[0] = int.class;

			Object[] parameters = new Object[1];
			parameters[0] = Integer.parseInt(catID);
			
			ws = new WebServices();

			try 
			{
				LoadingTask loadingTask = new LoadingTask(this, ws, WebServices.class.getMethod(
						"selectAllFromCategoryItemTableByID", parameterTypes), parameters, this);
				
				progressBar.setVisibility(View.VISIBLE);
				
				loadingTask.execute();
			}
			catch (SecurityException e) 
			{
				e.printStackTrace();
			} 
			catch (NoSuchMethodException e) 
			{
				e.printStackTrace();
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() 
	{
		// list = (ListView) findViewById(R.id.list2);

		// list.setAdapter(null);

		super.onDestroy();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
	{	
		Log.i("selected: ", position + 1 + "");

		@SuppressWarnings("unchecked")
		HashMap<String, String> item = (HashMap<String, String>) list.getAdapter().getItem(position);
		String itemName = item.get(KEY_NAME);
		String itemCode = item.get(KEY_CODE);
		String itemDesc = item.get(KEY_DESC);
		String itemMiddleImage = item.get(KEY_MIDDLE_IMAGE);
		String itemThumbPath = item.get(KEY_THUMBPATH);
		String itemID = item.get(KEY_ID);
		String itemTrade = "-";
		String itemImagePath1 = item.get(KEY_IMGPATH1);
		String itemImagePath2 = item.get(KEY_IMGPATH2);
		
		if(item.get(KEY_TRADE).length() > 0 && 
				!item.get(KEY_TRADE).toString().equals("Price on application"))
		{
			itemTrade = "$" + item.get(KEY_TRADE) + " " + item.get(KEY_TRADE_SUFFIX);
		}
		
		String itemPrice = "-";
				
		if(item.get(KEY_PRICE).length() > 0 && 
				!item.get(KEY_PRICE).toString().equals("Price on application"))
		{
			itemPrice = "$" + item.get(KEY_PRICE) + " " + item.get(KEY_PRICE_SUFFIX);
		}
		
		Intent intent = new Intent(ListOfItems.this, HardwareDetailActivity.class);

		Bundle bundle = new Bundle();
		bundle.putString("index", position + "");
		bundle.putString("hardwareTitle", itemName);
		bundle.putString("hardwareCode", itemCode);
		bundle.putString("hardwareTrade", itemTrade);
		bundle.putString("hardwareRetail", itemPrice);
		bundle.putString("colorIndex", colorIndex);
		bundle.putString("catName", catName);
		bundle.putString("hardwareDesc", itemDesc);
		bundle.putString("hardwareImagePath", itemMiddleImage);
		bundle.putString("hardwareThumbPath", itemThumbPath);
		bundle.putString("hardwareID", itemID);
		bundle.putString("hardwareGalleryImagePath", itemImagePath1);
		bundle.putString("hardwareTechnicalImagePath", itemImagePath2);
		
		intent.putExtras(bundle);

		startActivity(intent);
		overridePendingTransition(R.anim.right_enter,
				R.anim.right_exit);
	}

	@Override
	public void onTaskComplete(Object result) 
	{
		try 
		{
			progressBar.setVisibility(View.GONE);
			
			WebServices data = (WebServices) result;
			
			adapter = new ListOfItemsAdapter(this, data.wsObject, colorIndex);

			list.setAdapter(adapter);
			list.setOnItemClickListener(this);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.left_enter,
				R.anim.left_exit);
	}
}
