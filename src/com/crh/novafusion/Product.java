package com.crh.novafusion;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable{
	int MyjobID = 0;
	int ItemID = 0;
	String JobTitle = "";
	int ProductID = 0;
	String Title = "";
	int Quantity = 0;
	String ImagePathS = "";
	String ImagePathM = "";
	String Code = "";
	String Trade = "";
	String Retail;
	String Description = "";
	String ImageGalery = "";
	String ImageTechnical = "";

	public void setQuantity(int qt) {
		this.Quantity = qt;
	}

	public void setQuantity(String qt) {
		this.Quantity = Integer.parseInt(qt);
	}

	public void plusQuantity() {
		this.Quantity++;
	}

	public boolean minusQuantity() {
		if (this.Quantity != 0) {
			this.Quantity--;
			return true;
		}else
			return false;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

}
