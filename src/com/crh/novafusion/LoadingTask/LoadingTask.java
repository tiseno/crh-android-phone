package com.crh.novafusion.LoadingTask;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.crh.novafusion.WebServices;

public class LoadingTask extends AsyncTask<String, String, Object> {
	private AsyncTaskCompleteListener<Object> callback;
	private Activity activity;
	private Method mainTriggerMethod;
	private Object targetObject;
	private Object[] parameters;

	public LoadingTask(Activity activity, Object targetObject, Method method,
			Object[] parameters, AsyncTaskCompleteListener<Object> callback) {
		this.activity = activity;
		this.mainTriggerMethod = method;
		this.callback = callback;
		this.targetObject = targetObject;

		if (parameters == null) {
			this.parameters = null;
		} else {
			this.parameters = parameters;
		}
	}

	@Override
	protected Object doInBackground(String... params) {
		if (targetObject == null) {
			targetObject = new Object();
		}
		try {
			mainTriggerMethod.invoke(targetObject, parameters);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return targetObject;

	}

	protected void onPostExecute(Object result) {
		callback.onTaskComplete(result);
	}

}
