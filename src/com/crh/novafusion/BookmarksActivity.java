package com.crh.novafusion;

import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

public class BookmarksActivity extends Activity implements OnItemClickListener,
		OnClickListener {
	private ListView listview;
	private ProgressBar progressBar;
	private ArrayList<Product> data = new ArrayList<Product>();
	private Global global;

	private Button btEdit, btnSend;
	private LoadingTask loading;
	private Boolean isDeleteMode = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmarks);
		global = new Global(this);

		initView();
	}

	public ProgressBar getPreogressBar() {
		return this.progressBar;
	}

	public ListView getListView() {
		return listview;
	}

	private void initView() {
		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		listview = (ListView) findViewById(R.id.jobinfo_lv_listview);
		listview.setOnItemClickListener(this);

		btEdit = (Button) findViewById(R.id.jobinfo_bt_edit);
		btEdit.setOnClickListener(this);

		btnSend = (Button) findViewById(R.id.btn_send);
		btnSend.setOnClickListener(this);

		data = new ArrayList<Product>();
		loading = new LoadingTask();
		loading.execute();
	}

	private ArrayList<Product> getDataFromDB() {
		Cursor c = global.da.getAllBookmards();
		float totalRetail = 0;
		float totalTrade = 0;
		data.clear();

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			Product product = new Product();
			product.ProductID = c.getInt(c.getColumnIndex("BookmarkID"));
			product.ItemID = c.getInt(c.getColumnIndex("ItemID"));
			product.Title = c.getString(c.getColumnIndex("Title"));
			product.ImagePathS = c.getString(c.getColumnIndex("ImagePathS"));
			product.ImagePathM = c.getString(c.getColumnIndex("ImagePathM"));
			product.Code = c.getString(c.getColumnIndex("Code"));
			product.Description = c.getString(c.getColumnIndex("Description"));
			product.Retail = c.getString(c.getColumnIndex("Retail"));
			product.Trade = c.getString(c.getColumnIndex("Trade"));
			product.ImageGalery = c.getString(c.getColumnIndex("ImageGallery"));
			product.ImageTechnical = c.getString(c
					.getColumnIndex("ImageTechnical"));

			data.add(product);
		}
		return data;
	}

	public void proccessListData() {
		BookmarksAdapter adapter = new BookmarksAdapter(this, data);
		adapter.setIsDeleteMode(isDeleteMode);
		listview.setAdapter(adapter);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

	}

	@Override
	protected void onResume() {
		proccessListData();
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.jobinfo_bt_edit:

			BookmarksAdapter adapter = (BookmarksAdapter) listview.getAdapter();

			if (isDeleteMode) {
				isDeleteMode = false;
			} else {
				isDeleteMode = true;
			}

			adapter.setIsDeleteMode(isDeleteMode);
			listview.invalidateViews();

			break;

		case R.id.btn_send:

			// call from db all the bookmark details and pass it along
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList("bookmarks", data);
			bundle.putString("type", "bookmarks");
			bundle.putString("totalRetail", "-");
			bundle.putString("totalTrade", "-");

			// show the pop up button here
			global.popEmailDialog(BookmarksActivity.this, bundle);

			break;
		}
	}

	class LoadingTask extends AsyncTask<Integer, Integer, Object> {

		@Override
		protected void onPreExecute() {
			progressBar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected Object doInBackground(Integer... params) {

			return getDataFromDB();
		}

		@Override
		protected void onPostExecute(Object result) {
			proccessListData();
			progressBar.setVisibility(View.INVISIBLE);
			super.onPostExecute(result);
		}
	}

	@Override
	public void onBackPressed() {
		if (isDeleteMode) {
			BookmarksAdapter adapter = (BookmarksAdapter) listview.getAdapter();
			isDeleteMode = false;
			adapter.setIsDeleteMode(isDeleteMode);
			listview.invalidateViews();
			return;
		}
		super.onBackPressed();
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.no_anim, R.anim.slide_out_down);
	}
}
