package com.crh.novafusion;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

public class SendEnquiry extends Activity  implements AsyncTaskCompleteListener<Object>
{
	private EditText itemCode, firstName, lastName, email, phone, company, address, comments;
	private Bundle bundle;
	private String strItemCode, strColorIndex;
	private RelativeLayout layout;
	private String[] colors;
	private Button btnCancel, btnSend;
	private WebServices ws;
	private ProgressBar progressBar;
	private RadioButton rbtnSubscribe;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.send_enquiry);
		
		itemCode = (EditText) findViewById(R.id.edit_product_code);
		layout = (RelativeLayout) findViewById(R.id.send_enquiry_bg);
		btnCancel = (Button) findViewById(R.id.btn_enquiry_cancel);
		btnSend = (Button) findViewById(R.id.btn_enquiry_send);
		
		colors = this.getResources().getStringArray(R.array.catColor);
		ws = new WebServices();
		
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		firstName = (EditText) findViewById(R.id.edit_first_name);
		lastName = (EditText) findViewById(R.id.edit_last_name);
		email = (EditText) findViewById(R.id.edit_email);
		phone = (EditText) findViewById(R.id.edit_email);
		company = (EditText) findViewById(R.id.edit_company);
		address = (EditText) findViewById(R.id.edit_address);
		comments = (EditText) findViewById(R.id.edit_comment);
		rbtnSubscribe = (RadioButton) findViewById(R.id.radio_yes);
		
		bundle = this.getIntent().getExtras();
		strItemCode = bundle.getString("hardwareCode");
		strColorIndex = bundle.getString("colorIndex");
		
		itemCode.setText(strItemCode);
		layout.setBackgroundColor(Color.parseColor(colors[Integer
		           .parseInt(strColorIndex)]));
		
		btnCancel.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
		
		btnSend.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				//validation part
            	String strFirstName = firstName.getText().toString();
            	String strLastName = lastName.getText().toString();
            	String strEmail = email.getText().toString();
            	String strPhone = phone.getText().toString();
            	String strCompany = company.getText().toString();
            	String strAddress = address.getText().toString();
            	String strItemCode = itemCode.getText().toString();
            	String strComment = comments.getText().toString();
            	String strSubscribe = "no";
            	
            	if(rbtnSubscribe.isChecked())
            	{
            		strSubscribe = "yes";
            	}
				
				//after validation
				sendEnquiryFunction(strFirstName, strLastName, strEmail, strPhone, 
						strCompany, strAddress, strItemCode, strSubscribe, strComment);
			}
		});
	}

	public void sendEnquiryFunction(String strFirstName, String strLastName, String strEmail, String strPhone, 
			String strCompany, String strAddress, String strItemCode, String strSubscribe, String strComment)
	{
		try 
		{
			Class[] parameterTypes = new Class[9];
			parameterTypes[0] = String.class;
			parameterTypes[1] = String.class;
			parameterTypes[2] = String.class;
			parameterTypes[3] = String.class;
			parameterTypes[4] = String.class;
			parameterTypes[5] = String.class;
			parameterTypes[6] = String.class;
			parameterTypes[7] = String.class;
			parameterTypes[8] = String.class;

			Object[] parameters = new Object[9];
			parameters[0] = strFirstName;
			parameters[1] = strLastName;
			parameters[2] = strEmail;
			parameters[3] = strPhone;
			parameters[4] = strCompany;
			parameters[5] = strAddress;
			parameters[6] = strItemCode;
			parameters[7] = strSubscribe;
			parameters[8] = strComment;
			
			LoadingTask loadingTask = new LoadingTask(this, ws, WebServices.class.getMethod(
					"sendEnquiry", parameterTypes), parameters, this);
			
			progressBar.setVisibility(View.VISIBLE);
			
			loadingTask.execute();
		}
		catch (SecurityException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchMethodException e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void onTaskComplete(Object result) 
	{
		// TODO Auto-generated method stub
		progressBar.setVisibility(View.GONE);
		
		//show pop up here
		
		finish();
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.no_anim,
				R.anim.slide_out_down);
	}
}
