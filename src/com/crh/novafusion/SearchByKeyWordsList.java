package com.crh.novafusion;

import java.util.HashMap;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SearchByKeyWordsList extends Activity implements
		AsyncTaskCompleteListener<Object>, OnItemClickListener {
	private WebServices ws;
	private Bundle bundle;
	private ListView list;
	private SearchByKeyWordsListAdapter adapter;
	private TextView txtKeyWords, txtSearchTitle;
	private ProgressBar progressBar;
	private String keywords, itemName, itemCode, itemDesc, itemMiddleImage,
			itemThumbPath, itemID, itemTrade, itemPrice, mode, methodName,
			itemImagePath1, itemImagePath2;
	private boolean itemClicked, insideSearch;
	private int position;
	private EditText inputSearch;
	private Button btnSearch;
	private int x = 0;
	private Global global;

	static final String KEY_NAME = "name", KEY_TYPE = "type",
			KEY_APPID = "application_id", KEY_ID = "id", KEY_DESC = "desc",
			KEY_PRICE = "price", KEY_IMGPATH1 = "imagePath1",
			KEY_IMGPATH2 = "imagePath2", KEY_INCGST = "incgst",
			KEY_CODE = "code", KEY_THUMBPATH = "thumbPath",
			KEY_MIDDLE_IMAGE = "middleImage", KEY_TRADE = "trade",
			KEY_TRADE_SUFFIX = "tradeSuffix", KEY_PRICE_SUFFIX = "priceSuffix";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.search_items_list);
		global = new Global(this);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		list = (ListView) findViewById(R.id.searchItemListView);

		txtKeyWords = (TextView) findViewById(R.id.searchKeyWords);
		txtSearchTitle = (TextView) findViewById(R.id.txtSearch_title);

		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		inputSearch = (EditText) findViewById(R.id.inputSearch_2);

		btnSearch = (Button) findViewById(R.id.btn_search_2);

		ws = new WebServices();

		btnSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (x % 2 == 0) {
					txtSearchTitle.setVisibility(View.GONE);
					inputSearch.setVisibility(View.VISIBLE);
					inputSearch.requestFocus();
					global.showKeyboard();
				} else {
					txtSearchTitle.setVisibility(View.VISIBLE);
					inputSearch.setVisibility(View.GONE);
					inputSearch.clearFocus();
					global.hideKeyboard(v);

					inputSearch.setText("");
				}

				x++;
			}
		});

		itemClicked = false;
		insideSearch = false;

		try {
			bundle = this.getIntent().getExtras();

			mode = bundle.getString("mode");

			Class[] parameterTypes = new Class[1];
			Object[] parameters = new Object[1];

			if (mode.equals("keywords")) {
				keywords = bundle.getString("keywords");
				txtKeyWords.setText(keywords);

				parameterTypes[0] = String.class;

				parameters[0] = keywords;

				methodName = "searchItemByKeywords";
			} else {
				keywords = bundle.getString("result");
				txtKeyWords.setText(keywords);

				parameterTypes[0] = String.class;

				parameters[0] = keywords;

				methodName = "searchItemByBarCode";
			}

			try {
				LoadingTask loadingTask = new LoadingTask(
						this,
						ws,
						WebServices.class.getMethod(methodName, parameterTypes),
						parameters, this);

				progressBar.setVisibility(View.VISIBLE);

				loadingTask.execute();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		inputSearch.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					String strSearch = inputSearch.getText().toString();

					searchFunction(strSearch);
				}

				return false;
			}
		});
	}

	public void searchFunction(String strSearch) {
		insideSearch = true;

		Class[] parameterTypes = new Class[1];
		parameterTypes[0] = String.class;

		Object[] parameters = new Object[1];
		parameters[0] = strSearch;

		try {
			LoadingTask loadingTask2 = new LoadingTask(
					SearchByKeyWordsList.this, ws, WebServices.class.getMethod(
							"searchItemByKeywords", parameterTypes),
					parameters, this);

			progressBar.setVisibility(View.VISIBLE);

			loadingTask2.execute();

			txtKeyWords.setText(strSearch);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTaskComplete(Object result) {
		try {
			progressBar.setVisibility(View.GONE);

			if (itemClicked == false) {
				WebServices data = (WebServices) result;

				adapter = new SearchByKeyWordsListAdapter(this, data.wsObject,
						"");

				list.setAdapter(adapter);
				list.setOnItemClickListener(this);
			} else {
				if (insideSearch) {
					WebServices data = (WebServices) result;

					adapter = new SearchByKeyWordsListAdapter(this,
							data.wsObject, "");

					list.setAdapter(adapter);

					adapter.notifyDataSetChanged();
				} else {
					String colorIndex = "";

					for (int i = 0; i < ws.wsObject.size(); i++) {
						HashMap<String, String> dat = new HashMap<String, String>();
						dat = ws.wsObject.get(i);

						colorIndex = dat.get("ordering");
					}

					Intent intent = new Intent(SearchByKeyWordsList.this,
							HardwareDetailActivity.class);

					Bundle bundle = new Bundle();
					bundle.putString("index", this.position + "");
					bundle.putString("hardwareTitle", itemName);
					bundle.putString("hardwareCode", itemCode);
					bundle.putString("hardwareTrade", itemTrade);
					bundle.putString("hardwareRetail", itemPrice);
					bundle.putString("colorIndex", colorIndex);
					bundle.putString("hardwareDesc", itemDesc);
					bundle.putString("hardwareImagePath", itemMiddleImage);
					bundle.putString("hardwareThumbPath", itemThumbPath);
					bundle.putString("hardwareID", itemID);
					bundle.putString("hardwareGalleryImagePath", itemImagePath1);
					bundle.putString("hardwareTechnicalImagePath",
							itemImagePath2);

					intent.putExtras(bundle);

					startActivity(intent);
					overridePendingTransition(R.anim.right_enter,
							R.anim.right_exit);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		@SuppressWarnings("unchecked")
		HashMap<String, String> item = (HashMap<String, String>) list
				.getAdapter().getItem(position);
		itemName = item.get(KEY_NAME);
		itemCode = item.get(KEY_CODE);
		itemDesc = item.get(KEY_DESC);
		itemMiddleImage = item.get(KEY_MIDDLE_IMAGE);
		itemThumbPath = item.get(KEY_THUMBPATH);
		itemID = item.get(KEY_ID);
		itemImagePath1 = item.get(KEY_IMGPATH1);
		itemImagePath2 = item.get(KEY_IMGPATH2);
		itemTrade = "-";
		itemPrice = "-";
		itemClicked = true;
		this.position = position;

		if (item.get(KEY_TRADE).length() > 0
				&& !item.get(KEY_TRADE).toString()
						.equals("Price on application")) {
			itemTrade = "$" + item.get(KEY_TRADE) + " "
					+ item.get(KEY_TRADE_SUFFIX);
		}

		if (item.get(KEY_PRICE).length() > 0
				&& !item.get(KEY_PRICE).toString()
						.equals("Price on application")) {
			itemPrice = "$" + item.get(KEY_PRICE) + " "
					+ item.get(KEY_PRICE_SUFFIX);
		}

		try {
			Class[] parameterTypes = new Class[1];
			parameterTypes[0] = int.class;

			Object[] parameters = new Object[1];
			parameters[0] = Integer.parseInt(itemID);

			LoadingTask loadingTask = new LoadingTask(this, ws,
					WebServices.class.getMethod("getAppNameAndOrderByItemID",
							parameterTypes), parameters, this);

			progressBar.setVisibility(View.VISIBLE);

			loadingTask.execute();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.no_anim, R.anim.slide_out_down);
	}
}
