package com.crh.novafusion;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ListOfCategoryItems extends Activity implements AsyncTaskCompleteListener<Object>
{
	private ListView list;
	private CategoryAdapter adapter;
	private String catID, catName, colorIndex;
	private Bundle bundle;
	private WebServices ws;
	private TextView txtName;
	private LinearLayout ll;
	private ProgressBar progressBar;
	private int x = 0;

	private EditText inputSearch;
	private Button btnSearch;
	private InputMethodManager imm;

	static final String KEY_NAME = "name", KEY_PARENT = "parent",
			KEY_APPID = "application_id", KEY_ID = "id";

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.category_list);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		bundle = this.getIntent().getExtras();

		catID = bundle.getString("catID");
		catName = bundle.getString("catName");
		colorIndex = bundle.getString("colorIndex");

		inputSearch = (EditText) findViewById(R.id.inputSearch);
		
		inputSearch.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) 
			{
				// When user changed the Text
				adapter.getFilter().filter(cs.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) 
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) 
			{
				// TODO Auto-generated method stub
			}
		});

		txtName = (TextView) findViewById(R.id.txt_cat_name);
		txtName.setText(catName);

		ll = (LinearLayout) findViewById(R.id.linearLayout1);

		Global global = new Global(this);
		global.setTopbarColor(ll, Integer.parseInt(colorIndex));
		
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		
		btnSearch = (Button) findViewById(R.id.btn_Search);
		
		btnSearch.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				if(x % 2 == 0)
				{
					txtName.setVisibility(View.GONE);
					inputSearch.setVisibility(View.VISIBLE);
					inputSearch.requestFocus();
					
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
				}
				else
				{
					txtName.setVisibility(View.VISIBLE);
					inputSearch.setVisibility(View.GONE);
					inputSearch.clearFocus();
					
					imm.hideSoftInputFromWindow(v.getWindowToken(),0); 
					
					inputSearch.setText("");
				}
				
				x++;
			}
		});

		// desmond add
		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		Class[] parameterTypes = new Class[1];
		parameterTypes[0] = int.class;

		Object[] parameters = new Object[1];
		parameters[0] = Integer.parseInt(catID);

		//WebServices ws2 = new WebServices();
		ws = new WebServices();
		
		try 
		{
			LoadingTask loadingTask = new LoadingTask(this, ws, WebServices.class.getMethod(
				"selectAllFromCategoryTableExceptParam", parameterTypes), parameters, this);
			
			progressBar.setVisibility(View.VISIBLE);
			
			loadingTask.execute();
		} 
		catch (SecurityException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchMethodException e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() 
	{
		list = (ListView) findViewById(R.id.list2);

		list.setAdapter(null);

		super.onDestroy();
	}

	@Override
	public void onTaskComplete(Object result) 
	{
		progressBar.setVisibility(View.GONE);

		WebServices data = (WebServices) result;

		//Toast.makeText(this, "" + data.wsObject.size(), 1000).show();
		list = (ListView) findViewById(R.id.list2);
		
		adapter = new CategoryAdapter(ListOfCategoryItems.this, data.wsObject, colorIndex, catName);
		
		list.setAdapter(adapter);
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.left_enter,
				R.anim.left_exit);
	}
}
