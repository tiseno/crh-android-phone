package com.crh.novafusion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class WebServices 
{
	//public List<MainItems> mainListItem;
	public ArrayList<HashMap<String, String>> wsObject;
	//public List<CategoryItems> categoryListItem;
	public String catid_itemid, applicationName, specific_catID, specified_parentID, pdfStatus;
	//public ItemDetails ids;
	public HashMap<String, String> item;
	//public List<ItemDetails> itemList;
	public ArrayList<String> imagePath1_arr, imagePath2_arr, itemIDList; 
	
	//not sure whether itemIDLIst will be needed
	
	private Context mContext;
	
	public WebServices()
	{
		
	}
	
	public WebServices(Context context) 
	{
		this.mContext = context;
	}
	
	public ArrayList<HashMap<String, String>> selectAllFromApplicationTableExcepParam_Result(String result)
	{	
		try 
		{	
			JSONObject obj = new JSONObject(result);
						
			String obj_val = obj.getString("posts");
						
			JSONArray jsonArray = new JSONArray(obj_val);
			
			//mainListItem = new ArrayList<MainItems>();
				      
			wsObject = new ArrayList<HashMap<String, String>>();
			
			for (int i = 0; i < jsonArray.length(); i++) 
			{
			   	  JSONObject jsonObject = jsonArray.getJSONObject(i);
				    	  
			   	  JSONObject jsonObject2 = new JSONObject(jsonObject.getString("application"));
				    	  
			   	  JSONObject jsonObject3 = new JSONObject(jsonObject2.getString("params"));
				  
			   	  //MainItems mi = new MainItems();
			   	  
			   	  HashMap<String, String> item = new HashMap<String, String>();
			   	
			   	  //mi.setID(Integer.parseInt(jsonObject2.getString("id")));
			   	  //mi.setName(jsonObject2.getString("name"));
			   	  //mi.setDescription(jsonObject3.getString("content.teaser_description"));
			   	  
			   	  item.put("id", Integer.parseInt(jsonObject2.getString("id"))+"");
			   	  item.put("name", jsonObject2.getString("name"));
			   	  item.put("content.teaser_description", jsonObject3.getString("content.teaser_description"));
			   	  
			   	  if(jsonObject3.getString("content.teaser_image_width").equals(""))
			   	  {
			   		 //mi.setWidth(0);
			   		 
			   		  item.put("content.teaser_image_width", 0 + ""); 
			   	  }
			   	  else
			   	  {
			   		  //mi.setWidth(Integer.parseInt(jsonObject3.getString("content.teaser_image_width")));
			   		  
			   		  item.put("content.teaser_image_width", jsonObject3.getString("content.teaser_image_width")); 
			   	  }
			   	  
			   	  if(jsonObject3.getString("content.teaser_image_height").equals(""))
			   	  {
			   		  //mi.setHeight(0);
			   		  
			   		  item.put("content.teaser_image_height", jsonObject3.getString("content.teaser_image_height")); 
			   	  }
			   	  else
			   	  {
			   		  //mi.setHeight(Integer.parseInt(jsonObject3.getString("content.teaser_image_height")));
			   		  
			   		  item.put("content.teaser_image_height", jsonObject3.getString("content.teaser_image_height")); 
			   	  }
			   	  
			   	  if(jsonObject3.getString("content.teaser_image").equals(""))
			   	  {
			   		  //mi.setImagePath("");
			   		  
			   		  item.put("content.teaser_image", ""); 
			   	  }
			   	  else
			   	  {
			   		  //mi.setImagePath(jsonObject3.getString("content.teaser_image"));  
			   		
			   		  item.put("content.teaser_image", jsonObject3.getString("content.teaser_image"));
			   	  }
			   	  
			   	  //mainListItem.add(mi);
			   	  
			   	  wsObject.add(item);
				    	  
				}
		    } 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		//return mainListItem;
		
		return wsObject;
	}
	
	public void selectAllFromApplicationTableExcepParam()
	{
		/*
		 		WebServices ws = new WebServices();
            	ws.selectAllFromApplicationTableExcepParam();
            	
            	for(int i = 0; i < ws.mainListItem.size(); i++)
            	{
            		System.out.println("id : " + ws.mainListItem.get(i).getHeight());
            	}
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpGet httpGet = new HttpGet("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllFromApplicationTableExceptParam&format=json");
		    
					try 
					{
						HttpResponse response = client.execute(httpGet);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectAllFromApplicationTableExcepParam_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error ... " + ex.toString());
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectAllFromCategoryTableExceptParam_Result(String result)
	{
		try 
		{
			//categoryListItem = new ArrayList<CategoryItems>();
			wsObject = new ArrayList<HashMap<String, String>>();
			
			JSONObject obj = new JSONObject(result);
		
			String obj_val = obj.getString("posts");
					
			JSONArray jsonArray = new JSONArray(obj_val);
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				
				JSONObject jsonObject2 = new JSONObject(jsonObject.getString("category"));
				//JSONObject jsonObject3 = new JSONObject(jsonObject2.getString("params"));
				
				//CategoryItems ci = new CategoryItems();
				//ci.setID(Integer.parseInt(jsonObject2.getString("id")));
				//ci.setApplicationID(Integer.parseInt(jsonObject2.getString("application_id")));
				//ci.setParentID(Integer.parseInt(jsonObject2.getString("parent")));
				//ci.setName(jsonObject2.getString("name"));
				
				//categoryListItem.add(ci);
				
				HashMap<String, String> item = new HashMap<String, String>();
				item.put("id", jsonObject2.getString("id"));
				item.put("application_id", jsonObject2.getString("application_id"));
				item.put("parent", jsonObject2.getString("parent"));
				item.put("name", jsonObject2.getString("name"));
				
				wsObject.add(item);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
	}
	
	public void selectAllFromCategoryTableExceptParam(final int appID)
	{
		/*
		 	WebServices ws = new WebServices();
            ws.selectAllFromCategoryTableExceptParam(x);
            	
            for(int i = 0; i < ws.categoryListItem.size(); i++)
            {
            	System.out.println("name : " + ws.categoryListItem.get(i).getName());
            }
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("appID", String.valueOf(appID));
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllFromCategoryTableExceptParam&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectAllFromCategoryTableExceptParam_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectAllFromCategoryItemTableByID_Result(String result)
	{
		catid_itemid = "";
		
		catid_itemid = result;
		
		selectListOfCategoryItems(catid_itemid);
	}
	
	public void selectAllFromCategoryItemTableByID(final int catID)
	{
		/*
		 		WebServices ws = new WebServices();
            	ws.selectAllFromCategoryItemTableByID(151); 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("catID", String.valueOf(catID));
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllFromCategoryItemTableByID&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectAllFromCategoryItemTableByID_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectItemFromItemTableByID_Result(String result)
	{	
		try 
		{	
			JSONObject obj = new JSONObject(result);
			
			String obj_val = obj.getString("posts");
			
			JSONArray jsonArray = new JSONArray(obj_val);
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				//ids = new ItemDetails();
				
				item = new HashMap<String, String>();
				
				JSONObject obj2 = new JSONObject(jsonArray.getString(i));
				
				JSONObject obj3 = new JSONObject(obj2.getString("item"));
				
				//ids.setID(Integer.parseInt(obj3.getString("id")));
				//ids.setApplicationID(Integer.parseInt(obj3.getString("application_id")));
				//ids.setType(obj3.getString("type"));
				//ids.setName(obj3.getString("name"));
				
				item.put("id", obj3.getString("id"));
				item.put("application_id", obj3.getString("application_id"));
				item.put("type", obj3.getString("type"));
				item.put("name", obj3.getString("name"));
				
				JSONObject obj4 = new JSONObject(obj3.getString("elements"));
				
				JSONObject obj5 = new JSONObject(obj4.getString("98ac0b3a-035d-4367-97fb-033cd64f659a"));
				
				JSONObject obj6 = new JSONObject(obj5.getString("0"));
				
				//ids.setDescription(obj6.getString("value"));
				
				item.put("desc", obj6.getString("value"));
				
				JSONObject obj7 = new JSONObject(obj4.getString("db4b1af4-29b4-4432-8acb-5ade2510745a"));
				
				JSONObject obj8 = new JSONObject(obj7.getString("0"));
				
				//ids.setPrice(obj8.getString("value"));
				
				item.put("price", obj8.getString("value"));
				
				JSONObject obj9 = new JSONObject(obj4.getString("5f304277-9eff-4033-88b9-e231db615697"));
				
				//ids.setImagePath1(obj9.getString("value"));
				
				item.put("imagePath1", obj9.getString("value"));
				
				JSONObject obj10 = new JSONObject(obj4.getString("35edcd61-decc-4ba2-9146-995bcbcb0245"));
				
				//ids.setImagePath2(obj10.getString("value"));
				
				item.put("imagePath2", obj10.getString("value"));
				
				//JSONObject obj11 = new JSONObject(obj4.getString("695755a4-ae67-43bb-8c39-af4eb9ad4961"));
				//JSONObject obj12 = new JSONObject(obj11.getString("option"));
				//ids.setIncGst(obj12.getString("0"));
				//item.put("incgst", obj12.getString("0"));
				
				if(!obj4.isNull("695755a4-ae67-43bb-8c39-af4eb9ad4961"))
				{
					JSONObject obj11 = new JSONObject(obj4.getString("695755a4-ae67-43bb-8c39-af4eb9ad4961"));
					
					JSONObject obj12 = new JSONObject(obj11.getString("option"));
					
					item.put("incgst", obj12.getString("0"));
					
					if(obj12.isNull("0") == false && obj12.getString("0").length() != 0)
					{
						item.put("priceSuffix", "Incl GST");
					}
					else
					{
						item.put("priceSuffix", "Excl GST");
					}
				}
				else
				{	
					item.put("incgst", "-");
					item.put("priceSuffix", "Excl GST");
				}
				
				JSONObject obj13 = new JSONObject(obj4.getString("3e1dc070-343b-40cb-a8dc-b419d1a01b60"));
				
				JSONObject obj14 = new JSONObject(obj13.getString("0"));
				
				item.put("code", obj14.getString("value"));
				
				//
				Log.i("inside ws: code = ", obj14.getString("value"));
				//
				
				JSONObject obj15 = new JSONObject(obj4.getString("c0794696-a5d2-4df9-b20f-f23b0f94a41b"));
				
				//ids.setThumbPath("http://crh-app.novafusion.net:8052/" + obj9.getString("file"));
				
				item.put("thumbPath", "http://crh-app.novafusion.net:8052/" + obj15.getString("file"));
				
				JSONObject obj16 = new JSONObject(obj4.getString("cfca6d69-6dde-490f-a2ac-caa3b23921eb"));
				
				item.put("middleImage", "http://crh-app.novafusion.net:8052/" + obj16.getString("file"));
				
				if(obj4.isNull("581b36e3-59c1-458a-bdc4-8016209731e6") == false)
				{
					JSONObject obj17 = new JSONObject(obj4.getString("581b36e3-59c1-458a-bdc4-8016209731e6"));
					
					JSONObject obj18 = new JSONObject(obj17.getString("option"));
					
					//item.put("trade", obj18.getString("0"));
					
					//another component here
					if(obj18.isNull("0") == false && obj18.getString("0").length() != 0)
					{
						item.put("tradeSuffix", "Incl GST");
					}
					else
					{
						item.put("tradeSuffix", "Excl GST");
					}
				}
				else
				{	
					//item.put("trade", "-");
					item.put("tradeSuffix", "");
				}
				
				JSONObject obj19 = new JSONObject(obj4.getString("953ff3d0-753c-47f7-aeec-96599ecc4814"));
				
				JSONObject obj20 = new JSONObject(obj19.getString("0"));
				
				item.put("trade", obj20.getString("value"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public void selectItemFromItemTableByID(final int itemID)
	{
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("itemID", String.valueOf(itemID));
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectItemFromItemTableByID&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectItemFromItemTableByID_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectListOfCategoryItems(final String result)
	{
		/*
            ws.selectAllFromCategoryItemTableByID(150);
            	
            //pass the result to proceed the call
            ws.selectListOfCategoryItems(ws.catid_itemid);
            	
            for(int i = 0; i < ws.itemList.size(); i++)
            {
            	System.out.println("item name : " + ws.itemList.get(i).getName());
            } 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj1 = new JSONObject(result);
					
					JSONArray jsonArray = new JSONArray(obj1.getString("posts"));
					
					//itemList = new ArrayList<ItemDetails>();
					wsObject = new ArrayList<HashMap<String, String>>();
					
					for(int i = 0; i < jsonArray.length(); i++)
					{
						JSONObject obj2 = new JSONObject(jsonArray.get(i).toString());
						
						JSONObject obj3 = new JSONObject(obj2.getString("category_item"));
						
						//use the item id to get the item details
						selectItemFromItemTableByID(Integer.parseInt(obj3.getString("item_id")));
						
						//add item into the itemList
						//itemList.add(ids);
						
						wsObject.add(item);
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectGalleryImageByPath_Result(String result)
	{	
		try
		{
			imagePath1_arr = new ArrayList<String>();
			
			JSONObject obj = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj.getString("posts"));
			
			for(int i = 0; i < jsonArray.length(); i++)
			{	
				JSONObject obj2 = new JSONObject(jsonArray.get(i).toString());
				
				String temp = obj2.getString("img").replace("..", "http://crh-app.novafusion.net:8052");
				
				imagePath1_arr.add(temp);
			}
		}
		catch(Exception e)
		{
			System.err.println("error : " + e.toString());
		}
	}
	
	public void selectGalleryImageByPath(final String imagePath)
	{
		/*
		 	String str = "/refrigeration_hardware/temperature_gauges_thermometers/92220308/gallery";
		 	
		 	WebServices ws = new WebServices();
            ws.selectGalleryImageByPath(str);
            	
            for(int i = 0; i < ws.imagePath1_arr.size(); i++)
            {
            	System.out.println("data - " + ws.imagePath1_arr.get(i));
            }
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("imagePath", imagePath);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectGalleryImageByPath&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectGalleryImageByPath_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectTechnicalImageByPath_Result(String result)
	{	
		try
		{
			imagePath2_arr = new ArrayList<String>();
			
			JSONObject obj = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj.getString("posts"));
			
			for(int i = 0; i < jsonArray.length(); i++)
			{	
				JSONObject obj2 = new JSONObject(jsonArray.get(i).toString());
				
				String temp = obj2.getString("img").replace("..", "http://crh-app.novafusion.net:8052");
				
				imagePath2_arr.add(temp);
			}
		}
		catch(Exception e)
		{
			System.err.println("error : " + e.toString());
		}
	}
	
	public void selectTechnicalImageByPath(final String imagePath)
	{
		/*
		 	String str = "/polyurethane_insulated_doors_and_frame_kits/sliding_door_frame_kits/SDAFK182075/technical";
            	
            WebServices ws = new WebServices();
            ws.selectTechnicalImageByPath(str);
            	
            for(int i = 0; i < ws.imagePath2_arr.size(); i++)
            {
            	System.out.println("data - " + ws.imagePath2_arr.get(i));
            }
		 */
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("imagePath", imagePath);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectTechnicalImageByPath&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectTechnicalImageByPath_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void sendEnquiry(final String FirstName, final String LastName, final String Email, final String Phone, final String Company, final String Address, final String ProductCode, final String Subscribe, final String Comments)
	{
		/*
		 	WebServices ws = new WebServices();
            	
            ws.sendEnquiry("First Name", "Last Name", "rexxenckc@msn.com", "012 78787878", "Company", "Address", "ProductCode", "Subscribe", "Comments");
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("FirstName", FirstName);
					obj.put("LastName", LastName);
					obj.put("Email", Email);
					obj.put("Phone", Phone);
					obj.put("Company", Company);
					obj.put("Address", Address);
					obj.put("ProductCode", ProductCode);
					obj.put("Subscribe", Subscribe);
					obj.put("Comments", Comments);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=sendEnquiry&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							generatePDF_File_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void generatePDF_File_Result(String result)
	{
		pdfStatus = "";
		
		pdfStatus = result;
	}
	
	public void generatePDF_FileWithPrice(final String Title, final String Code, final String Trade, final String Retail, final String Description, final String ImagePath, final String TimeStamp)
	{
		/*
		 	String Title = "92220308 Drum Type Temperature Gauge. Temp. Range: -40 Celcius to +40 Celcius";
            String Code = "Code : 92220308";
            String Trade = "Trade : -";
            String Retail = "Retail : $47.34 Incl GST";
            String Description = "Drum type temperature gauge. 2m capillary. Mounting: Recess. Temperature Range: -40Celcius to +40Celcius";
            String ImagePath = "../images/products/refrigeration_hardware/temperature_gauges_thermometers/92220308/full/92220308.jpg";
            String TimeStamp = "1359427708";
            	
            WebServices ws = new WebServices();
            ws.generatePDF_FileWithPrice(Title, Code, Trade, Retail, Description, ImagePath, TimeStamp);
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("Title", Title);
					obj.put("Code", Code);
					obj.put("Trade", Trade);
					obj.put("Retail", Retail);
					obj.put("Description", Description);
					obj.put("ImagePath", ImagePath);
					obj.put("TimeStamp", TimeStamp);
					
					System.out.println("nani ... " + obj.getString("Title"));
					System.out.println("nani ... " + Code);
					System.out.println("nani ... " + Trade);
					System.out.println("nani ... " + Retail);
					System.out.println("nani ... " + obj.getString("Description"));
					System.out.println("nani ... " + ImagePath);
					System.out.println("nani ... " + TimeStamp);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generatePDF_FileWithPrice&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							Log.i("generatePDF_File_Result", builder.toString());
							generatePDF_File_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void generatePDF_FileWithoutPrice(final String Title, final String Code, final String Description, final String ImagePath, final String TimeStamp)
	{
		/*
		 	String Title = "92220308 Drum Type Temperature Gauge. Temp. Range: -40 Celcius to +40 Celcius";
            String Code = "Code : 92220308";
            String Description = "Drum type temperature gauge. 2m capillary. Mounting: Recess. Temperature Range: -40Celcius to +40Celcius";
            String ImagePath = "../images/products/refrigeration_hardware/temperature_gauges_thermometers/92220308/full/92220308.jpg";
            String TimeStamp = "1359427708";
            	
            WebServices ws = new WebServices();
            ws.generatePDF_FileWithoutPrice(Title, Code, Description, ImagePath, TimeStamp);
            	
            System.out.println("status : " + ws.pdfStatus);
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("Title", Title);
					obj.put("Code", Code);
					obj.put("Description", Description);
					obj.put("ImagePath", ImagePath);
					obj.put("TimeStamp", TimeStamp);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generatePDF_FileWithoutPrice&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							generatePDF_File_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void searchItemByKeywords_Result(final String result)
	{	
		try
		{
			JSONObject obj = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj.getString("posts"));
			
			//itemList = new ArrayList<ItemDetails>();
			wsObject = new ArrayList<HashMap<String, String>>();
			
			for(int i = 0; i < jsonArray.length(); i++)
			{	
				//ids = new ItemDetails();
				
				item = new HashMap<String, String>();
				
				JSONObject obj2 = new JSONObject(jsonArray.get(i).toString());
				
				JSONObject obj3 = new JSONObject( obj2.getString("item"));
				
				//ids.setName(obj3.getString("name"));
				//ids.setApplicationID(Integer.parseInt(obj3.getString("application_id")));
				//ids.setID(Integer.parseInt(obj3.getString("id")));
				
				item.put("name", obj3.getString("name"));
				item.put("application_id", obj3.getString("application_id"));
				item.put("id", obj3.getString("id"));
				
				JSONObject obj4 = new JSONObject(obj3.getString("elements"));
				
				JSONObject obj5 = new JSONObject(obj4.getString("db4b1af4-29b4-4432-8acb-5ade2510745a"));
				
				JSONObject obj6 = new JSONObject(obj5.getString("0"));
				
				//ids.setPrice(obj6.getString("value"));
				
				item.put("price", obj6.getString("value"));
				
				JSONObject obj7 = new JSONObject(obj4.getString("3e1dc070-343b-40cb-a8dc-b419d1a01b60"));
				
				JSONObject obj8 = new JSONObject(obj7.getString("0"));
				
				//ids.setCode(obj8.getString("value"));
				
				item.put("code", obj8.getString("value"));
				
				JSONObject obj9 = new JSONObject(obj4.getString("c0794696-a5d2-4df9-b20f-f23b0f94a41b"));
				
				//ids.setThumbPath("http://crh-app.novafusion.net:8052/" + obj9.getString("file"));
				
				item.put("thumbPath", "http://crh-app.novafusion.net:8052/" + obj9.getString("file"));
				
				if(obj4.isNull("581b36e3-59c1-458a-bdc4-8016209731e6") == false)
				{
					JSONObject obj10 = new JSONObject(obj4.getString("581b36e3-59c1-458a-bdc4-8016209731e6"));
					
					JSONObject obj11 = new JSONObject(obj10.getString("option"));
					
					//ids.setTrade(obj11.getString("0"));
					//item.put("trade", obj11.getString("0"));
					
					if(obj11.isNull("0") == false && obj11.getString("0").length() != 0)
					{
						item.put("tradeSuffix", "Incl GST");
					}
					else
					{
						item.put("tradeSuffix", "Excl GST");
					}
				}
				else
				{
					//ids.setTrade("-");
					//item.put("trade", "-");
					
					item.put("tradeSuffix", "");
				}
				
				JSONObject obj12 = new JSONObject(obj4.getString("953ff3d0-753c-47f7-aeec-96599ecc4814"));
				
				JSONObject obj13 = new JSONObject(obj12.getString("0"));
				
				item.put("trade", obj13.getString("value"));
				
				
				if(!obj4.isNull("695755a4-ae67-43bb-8c39-af4eb9ad4961"))
				{
					JSONObject obj14 = new JSONObject(obj4.getString("695755a4-ae67-43bb-8c39-af4eb9ad4961"));
					
					JSONObject obj15 = new JSONObject(obj14.getString("option"));
					
					item.put("incgst", obj15.getString("0"));
					
					if(obj15.isNull("0") == false && obj15.getString("0").length() != 0)
					{
						item.put("priceSuffix", "Incl GST");
					}
					else
					{
						item.put("priceSuffix", "Excl GST");
					}
				}
				else
				{	
					item.put("incgst", "-");
					item.put("priceSuffix", "Excl GST");
				}
				
				//itemList.add(ids);
				
				JSONObject obj16 = new JSONObject(obj4.getString("98ac0b3a-035d-4367-97fb-033cd64f659a"));
				
				JSONObject obj17 = new JSONObject(obj16.getString("0"));
				
				item.put("desc", obj17.getString("value"));
				
				JSONObject obj18 = new JSONObject(obj4.getString("cfca6d69-6dde-490f-a2ac-caa3b23921eb"));
				
				item.put("middleImage", "http://crh-app.novafusion.net:8052/" + obj18.getString("file"));
				
				wsObject.add(item);
				
				JSONObject obj19 = new JSONObject(obj4.getString("5f304277-9eff-4033-88b9-e231db615697"));
				
				item.put("imagePath1", obj19.getString("value"));
				
				JSONObject obj20 = new JSONObject(obj4.getString("35edcd61-decc-4ba2-9146-995bcbcb0245"));
				
				item.put("imagePath2", obj20.getString("value"));
			}
		}
		catch(Exception e)
		{
			System.err.println("error : " + e.toString());
		}
	}
	
	public void searchItemByKeywords(final String SearchStr)
	{
		/*
		 		String str = "slide";
            	
            	WebServices ws = new WebServices();
            	ws.searchItemByKeywords(str);
            	
            	for(int i = 0; i < ws.itemList.size(); i++)
            	{
            		System.out.println("id : " + ws.itemList.get(i).getID());
            		System.out.println("name : " + ws.itemList.get(i).getName());
            		System.out.println("app id : " + ws.itemList.get(i).getApplicationID());
            		System.out.println("price : " + ws.itemList.get(i).getPrice());
            		System.out.println("code : " + ws.itemList.get(i).getCode());
            		System.out.println("thumb path : " + ws.itemList.get(i).getThumbPath());
            		System.out.println("trade : " + ws.itemList.get(i).getTrade());
            	} 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("SearchStr", SearchStr);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=searchItemByKeywords&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							searchItemByKeywords_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectAllCategoryItem_Result(String result)
	{
		try
		{
			JSONObject obj1 = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj1.getString("posts"));
			
			itemIDList = new ArrayList<String>();
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject obj2 = jsonArray.getJSONObject(i);
			
				JSONObject obj3 = new JSONObject(obj2.getString("item"));
				
				itemIDList.add(obj3.getString("item_id"));
			}
		}
		catch(Exception e)
		{
			System.err.println("error !");
		}
	}
	
	public void selectAllCategoryItem(final int itemID)
	{
		/*
		 	WebServices ws = new WebServices();
            ws.selectAllCategoryItem(1631); 
            	
            for(int i = 0; i < ws.itemIDList.size(); i++)
            {
            	System.out.println("item id : " + ws.itemIDList.get(i).toString());
            } 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("itemID", itemID);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllCategoryItem&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectAllCategoryItem_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectApplicationNameByID_Result(String result)
	{
		applicationName = "";
		
		try
		{
			JSONObject obj1 = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj1.getString("posts"));
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject obj2 = jsonArray.getJSONObject(i);
				
				JSONObject obj3 = new JSONObject(obj2.getString("app"));
				
				applicationName = obj3.getString("name");
			}
		}
		catch(Exception e)
		{
			
		}
	}
	
	public void selectApplicationNameByID(final int appID)
	{
		/*
		 	WebServices ws = new WebServices();
            ws.selectApplicationNameByID(200);
            	
            System.out.println("app name : " + ws.applicationName); 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("appID", appID);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectApplicationNameByID&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectApplicationNameByID_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void selectCategoryIDByID_Result(String result)
	{	
		specific_catID = "";
		
		try
		{
			JSONObject obj1 = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj1.getString("posts"));
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject obj2 = jsonArray.getJSONObject(i);
				
				JSONObject obj3 = new JSONObject(obj2.getString("cat"));
				
				specific_catID = obj3.getString("category_id");
			}
			
		}
		catch(Exception e)
		{
			System.err.println("error !!");
		}
	}
	
	public void selectCategoryIDByID(final int itemID)
	{
		/*
		 	WebServices ws = new WebServices();
            ws.selectCategoryIDByID(1877);
            	
            System.out.println("nani ... " + ws.specific_catID); 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("itemID", itemID);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectCategoryIDByID&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							selectCategoryIDByID_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void generateListOfItemsPDFWithPrice(final String requestString)
	{
		/*
		 	String requestString = "[{\"Title\":\"92220308 Drum Type Temperature Gauge. Temp. Range: -40 Celcius to +40 Celcius\",\"Code\":\"92220308\",\"Trade\":\"-\",\"Retail\":\"$47.34 Incl GST\",\"Description\":\"Drum type temperature gauge. 2m capillary. Mounting: Recess. Temperature Range: -40Celcius to +40Celcius\",\"ImagePath\":\"../images/products/refrigeration_hardware/temperature_gauges_thermometers/92220308/full/92220308.jpg\",\"TimeStamp\":\"1359431175\",\"Quantity\":\"1\",\"TotalTrade\":\"88.48\",\"TotalRetail\":\"131.82\"},{\"Title\":\"F8760R Round Temperature Gauge. 60mm diameter. Temp. Range: -40celciusSign to +40celciusSign\",\"Code\":\"F8760R\",\"Trade\":\"$88.48 Incl GST\",\"Retail\":\"$84.48 Incl GST\",\"Description\":\"Round temperature gauge. 60mm diameter. 1.5m capillary. Mounting: Recess or Panel. Temperature Range: -40celciusSign to +40celciusSign\",\"ImagePath\":\"../images/products/refrigeration_hardware/temperature_gauges_thermometers/F8760R/full/F8760R.jpg\",\"TimeStamp\":\"1359431175\",\"Quantity\":\"1\",\"TotalTrade\":\"88.48\",\"TotalRetail\":\"131.82\"}]";
            	
            WebServices ws = new WebServices();
            ws.generateListOfItemsPDFWithPrice(requestString);
            	
            System.out.println("status : " + ws.pdfStatus);		 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generateListOfItemsPDFWithPrice&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(requestString);
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							generatePDF_File_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void generateListOfItemsPDFWithoutPrice(final String requestString)
	{
		/*
		 	String requestString = "[{\"Title\":\"92220308 Drum Type Temperature Gauge. Temp. Range: -40 Celcius to +40 Celcius\",\"Code\":\"92220308\",\"Description\":\"Drum type temperature gauge. 2m capillary. Mounting: Recess. Temperature Range: -40Celcius to +40Celcius\",\"ImagePath\":\"../images/products/refrigeration_hardware/temperature_gauges_thermometers/92220308/full/92220308.jpg\",\"TimeStamp\":\"1359435199\",\"Quantity\":\"1\"},{\"Title\":\"F8760R Round Temperature Gauge. 60mm diameter. Temp. Range: -40celciusSign to +40celciusSign\",\"Code\":\"F8760R\",\"Description\":\"Round temperature gauge. 60mm diameter. 1.5m capillary. Mounting: Recess or Panel. Temperature Range: -40celciusSign to +40celciusSign\",\"ImagePath\":\"../images/products/refrigeration_hardware/temperature_gauges_thermometers/F8760R/full/F8760R.jpg\",\"TimeStamp\":\"1359435199\",\"Quantity\":\"1\"}]";
            	
            WebServices ws = new WebServices();
            ws.generateListOfItemsPDFWithoutPrice(requestString);
            	
            System.out.println("status : " + ws.pdfStatus);
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generateListOfItemsPDFWithoutPrice&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(requestString);
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							generatePDF_File_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void searchItemByBarCode_Result(String result)
	{		
		try
		{
			//itemList = new ArrayList<ItemDetails>();
			wsObject = new ArrayList<HashMap<String, String>>();
			
			JSONObject obj1 = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj1.getString("posts"));
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				//ids = new ItemDetails();
				
				item = new HashMap<String, String>();
				
				JSONObject obj2 = jsonArray.getJSONObject(i);
				
				JSONObject obj3 = new JSONObject(obj2.getString("item"));
				
				//ids.setID(Integer.parseInt(obj3.getString("id")));
				//ids.setApplicationID(Integer.parseInt(obj3.getString("application_id")));
				//ids.setName(obj3.getString("name"));
				
				item.put("id", obj3.getString("id"));
				item.put("application_id", obj3.getString("application_id"));
				item.put("name", obj3.getString("name"));
				
				JSONObject obj4 = new JSONObject(obj3.getString("elements"));
				
				JSONObject obj5 = new JSONObject(obj4.getString("db4b1af4-29b4-4432-8acb-5ade2510745a"));
				
				JSONObject obj6 = new JSONObject(obj5.getString("0"));
				
				//ids.setPrice(obj6.getString("value"));
				
				item.put("price", obj6.getString("value"));
				
				JSONObject obj7 = new JSONObject(obj4.getString("3e1dc070-343b-40cb-a8dc-b419d1a01b60"));
				
				JSONObject obj8 = new JSONObject(obj7.getString("0"));
				
				//ids.setCode(obj8.getString("value"));
				
				item.put("code", obj8.getString("value"));
				
				JSONObject obj9 = new JSONObject(obj4.getString("c0794696-a5d2-4df9-b20f-f23b0f94a41b"));
				
				//ids.setThumbPath("http://crh-app.novafusion.net:8052/" + obj9.getString("file"));
				
				item.put("thumbPath", "http://crh-app.novafusion.net:8052/" + obj9.getString("file"));
				
				//itemList.add(ids);
				
				if(obj4.isNull("581b36e3-59c1-458a-bdc4-8016209731e6") == false)
				{
					JSONObject obj10 = new JSONObject(obj4.getString("581b36e3-59c1-458a-bdc4-8016209731e6"));
					
					JSONObject obj11 = new JSONObject(obj10.getString("option"));
					
					//ids.setTrade(obj11.getString("0"));
					//item.put("trade", obj11.getString("0"));
					
					if(obj11.isNull("0") == false && obj11.getString("0").length() != 0)
					{
						item.put("tradeSuffix", "Incl GST");
					}
					else
					{
						item.put("tradeSuffix", "Excl GST");
					}
				}
				else
				{
					//ids.setTrade("-");
					//item.put("trade", "-");
					
					item.put("tradeSuffix", "");
				}
				
				JSONObject obj12 = new JSONObject(obj4.getString("953ff3d0-753c-47f7-aeec-96599ecc4814"));
				
				JSONObject obj13 = new JSONObject(obj12.getString("0"));
				
				item.put("trade", obj13.getString("value"));
				
				if(!obj4.isNull("695755a4-ae67-43bb-8c39-af4eb9ad4961"))
				{
					JSONObject obj14 = new JSONObject(obj4.getString("695755a4-ae67-43bb-8c39-af4eb9ad4961"));
					
					JSONObject obj15 = new JSONObject(obj14.getString("option"));
					
					item.put("incgst", obj15.getString("0"));
					
					if(obj15.isNull("0") == false && obj15.getString("0").length() != 0)
					{
						item.put("priceSuffix", "Incl GST");
					}
					else
					{
						item.put("priceSuffix", "Excl GST");
					}
				}
				else
				{	
					item.put("incgst", "-");
					item.put("priceSuffix", "Excl GST");
				}
				
				//itemList.add(ids);
				
				JSONObject obj16 = new JSONObject(obj4.getString("98ac0b3a-035d-4367-97fb-033cd64f659a"));
				
				JSONObject obj17 = new JSONObject(obj16.getString("0"));
				
				item.put("desc", obj17.getString("value"));
				
				JSONObject obj18 = new JSONObject(obj4.getString("cfca6d69-6dde-490f-a2ac-caa3b23921eb"));
				
				item.put("middleImage", "http://crh-app.novafusion.net:8052/" + obj18.getString("file"));
				
				wsObject.add(item);
			}
		}
		catch(Exception e)
		{
			System.err.println("error 1" + e.toString());
		}
	}
	
	public void searchItemByBarCode(final String BarCode)
	{
		/*
		 	WebServices ws = new WebServices();
            ws.test();
            ws.searchItemByBarCode("92220308");
            	
            for(int i = 0; i < ws.itemList.size(); i++)
            {
            	System.out.println("id ... " + ws.itemList.get(i).getID());
            	System.out.println("app id ... " + ws.itemList.get(i).getApplicationID());
            	System.out.println("name ... " + ws.itemList.get(i).getName());
           		System.out.println("thumb ... " + ws.itemList.get(i).getThumbPath());
            	System.out.println("code ... " + ws.itemList.get(i).getCode());
            	System.out.println("price ... " + ws.itemList.get(i).getPrice());
            } 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("BarCode", BarCode);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=searchItemByBarCode&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							searchItemByBarCode_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void getParentKeyByItemID_Result(String result)
	{	
		specified_parentID = "";
		
		try
		{
			JSONObject obj1 = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj1.getString("posts"));
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject obj2 = jsonArray.getJSONObject(i);
				
				JSONObject obj3 = new JSONObject(obj2.getString("parentid"));
				
				specified_parentID = obj3.getString("parent");
			}
		}
		catch(Exception e)
		{
			System.err.println("error !");
		}
	}
	
	public void getParentKeyByItemID(final String ParentKey)
	{
		/*
		 	WebServices ws = new WebServices();
            ws.getParentKeyByItemID("209");
            	
            System.out.println("parend id : " + ws.specified_parentID); 
		 */
		
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("ParentKey", ParentKey);
					
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=getParentKeyByItemID&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							getParentKeyByItemID_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void getAppNameAndOrderByItemID_Result(String result)
	{	
		wsObject = new ArrayList<HashMap<String, String>>();
		
		try
		{
			JSONObject obj1 = new JSONObject(result);
			
			JSONArray jsonArray = new JSONArray(obj1.getString("posts"));
			
			for(int i = 0; i < jsonArray.length(); i++)
			{
				item = new HashMap<String, String>();
				
				JSONObject obj2 = jsonArray.getJSONObject(i);
				
				JSONObject obj3 = new JSONObject(obj2.getString("itemDetails"));
						
				item.put("ordering", obj3.getString("ordering"));
				item.put("name", obj3.getString("name"));
				
				wsObject.add(item);
			}
		}
		catch(Exception e)
		{
			System.err.println("error !");
		}
	}
	
	public void getAppNameAndOrderByItemID(final int ItemID)
	{
		Thread networkThread = new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					JSONObject obj = new JSONObject();
					obj.put("ItemID", ItemID);
					Log.i("ItemID", ItemID+"");
					StringBuilder builder = new StringBuilder();
					
					HttpClient client = new DefaultHttpClient();
		 
					HttpPost httpPost = new HttpPost("http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=getAppNameAndOrderByItemID&format=json");
					httpPost.setHeader("Content-type", "application/json");
					
					StringEntity se = new StringEntity(obj.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
					httpPost.setEntity(se);
					
					try 
					{
						HttpResponse response = client.execute(httpPost);
		      
						StatusLine statusLine = response.getStatusLine();
		      
						int statusCode = statusLine.getStatusCode();
		      
						if (statusCode == 200) 
						{
							HttpEntity entity = response.getEntity();
		        
							InputStream content = entity.getContent();
		        
							BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        
							String line;
		        
							while ((line = reader.readLine()) != null) 
							{
								builder.append(line);
							}
							
							getAppNameAndOrderByItemID_Result(builder.toString());
						} 
						else 
						{
							Log.e(MainActivity.class.toString(), "Failed to download file");
						}
					}	 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
				catch(Exception ex)
				{
					System.out.println("error 1!");
				}
			}
		};
			networkThread.start();
			
			try{networkThread.join();}catch(Exception e){}
	}
	
	public void test()
	{
		System.out.println("test");
	}
}
