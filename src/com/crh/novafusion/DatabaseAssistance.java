package com.crh.novafusion;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAssistance {
	private String myjob_table = DBConnection.MYJOB_TABLE;
	private String bookmark_table = DBConnection.BOOMARK_TABLE;
	private String product_table = DBConnection.PRODUCT_TABLE;

	private SQLiteDatabase mDb;

	public DatabaseAssistance(SQLiteDatabase db) {
		mDb = db;
	}

	public Cursor getAllJob() {
		return mDb.query(myjob_table, null, null, null, null, null, null);
	}

	public Cursor getAllBookmards() {
		return mDb.query(bookmark_table, null, null, null, null, null, null);
	}

	public Cursor getAllProduct() {
		return mDb.query(product_table, null, null, null, null, null, null);
	}

	public Cursor getProductByCode(String code) {
		// String sql =
		// "SELECT * FROM Myjob LEFT JOIN Product ON Myjob.MyjobID = Product.Jobid AND Code ="+
		// code +";";
		String sql = "SELECT m.MyjobID, m.Title as JobTitle, p.ProductID,p.Title, p.Quantity " +
				"FROM Myjob as m LEFT JOIN Product as p ON m.MyjobID = p.Jobid AND Code='"+ code +"';";
		
		return mDb.rawQuery(sql, null);
	}
	
	public Cursor getProductByJobid(int jobid){
		String select = "Jobid=" + jobid + "";
		return mDb.query(product_table, null, select, null, null, null, null);
	}

	public Cursor getJobByJobID(int id) {
		String select = "MyjobID=" + id + "";
		return mDb.query(myjob_table, null, select, null, null, null, null);
	}

	public Cursor getBookmardByBookmardID(int id) {
		String select = "BookmarkID=" + id + "";
		return mDb.query(bookmark_table, null, select, null, null, null, null);
	}

	public Cursor getProductByPruductID(int id) {
		String select = "ProductID=" + id + "";
		return mDb.query(product_table, null, select, null, null, null, null);
	}

	// insert job
	public long insertMyjob(ContentValues cv) {
		long result;
		try {
			result = mDb.insertOrThrow(myjob_table, "", cv);
		} catch (SQLiteConstraintException e) {
			e.printStackTrace();
			updateMyjob(cv, cv.getAsInteger("MyjobID"));
			result = 0;
		}
		return result;
	}

	public float updateMyjob(ContentValues cv, int id) {
		float result;
		result = mDb.update(myjob_table, cv, "MyjobID=" + id, null);

		return result;
	}
	
	public void DeleteMyjob(int id) {
		String[] parms = new String[] { id + "" };
		mDb.delete(myjob_table, "MyjobID=?", parms);
	}

	// insert bookmark
	public boolean insertBookmark(ContentValues cv) {
		long result;
		try {
			result = mDb.insertOrThrow(bookmark_table, "", cv);
		} catch (SQLiteConstraintException e) {
			e.printStackTrace();
			updateBookmark(cv, cv.getAsInteger("BookmarkID"));
			result = 0;
		}
		return result > 0;
	}

	public float updateBookmark(ContentValues cv, int id) {
		float result;
		result = mDb.update(bookmark_table, cv, "BookmarkID=" + id, null);

		return result;
	}
	
	public void DeleteBookmark(int id) {
		String[] parms = new String[] { id + "" };
		mDb.delete(bookmark_table, "BookmarkID=?", parms);
	}

	// insert product
	public long insertProduct(ContentValues cv) {
		long result;
		try {
			result = mDb.insertOrThrow(product_table, "", cv);
		} catch (SQLiteConstraintException e) {
			e.printStackTrace();
			updateProduct(cv, cv.getAsInteger("ProductID"));
			result = 0;
		}
		return result;
	}

	public float updateProduct(ContentValues cv, int id) {
		float result;
		result = mDb.update(product_table, cv, "ProductID=" + id, null);

		return result;
	}
	
	public float updateProduct(ContentValues cv, String code) {
		float result;
		result = mDb.update(product_table, cv, "Code=" + code, null);

		return result;
	}
	
	public void DeleteProduct(int id) {
		String[] parms = new String[] { id + "" };
		mDb.delete(product_table, "ProductID=?", parms);
	}
}
