package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;

import com.crh.novafusion.imageloader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListOfItemsAdapter extends BaseAdapter
{
	private static LayoutInflater inflater = null;
	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private String index;
	private String[] colors;
    private Drawable tri;
    
    private ImageLoader imageLoader;
    
	public ListOfItemsAdapter(Activity activity, ArrayList<HashMap<String, String>> data, String index)
	{
		this.activity = activity;
		
		this.data = data;
		
		this.index = index;
		
		inflater = (LayoutInflater)this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		colors = activity.getResources().getStringArray(R.array.catColor);
		
		TypedArray imgs;
		imgs = activity.getResources().obtainTypedArray(R.array.tri);
		tri = activity.getResources().getDrawable(imgs.getResourceId(Integer.parseInt(index), 0));
		
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}
	
	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		// TODO Auto-generated method stub
		
		View vi = convertView;
		viewHolder holder;
		
		if(convertView == null)
	     {
	    	holder = new viewHolder();
	        
	    	vi = inflater.inflate(R.layout.item_list_row, null);
	    	vi.setTag(holder);	
        }
	     else
	     {
	    	 holder = (viewHolder)convertView.getTag();
	     }
		
		 holder.name = (TextView)vi.findViewById(R.id.textView_name_item);
		 holder.code = (TextView)vi.findViewById(R.id.textView_code_item);
		 holder.desc = (TextView)vi.findViewById(R.id.textView_desc_item);
		 holder.number = (TextView)vi.findViewById(R.id.textView_item_numbering);
		 holder.thumbView = (ImageView)vi.findViewById(R.id.item_list_row_iv);
		 holder.tri = (ImageView)vi.findViewById(R.id.item_list_row_tri);
		 holder.tri.setImageDrawable(tri);
		 
		 HashMap<String, String> dat = new HashMap<String, String>();
	     dat = data.get(position);
	     
	     holder.name.setText(dat.get(ListOfItemsImages.KEY_NAME));
	     holder.code.setText(dat.get(ListOfItemsImages.KEY_CODE));
	     holder.desc.setText(Html.fromHtml(dat.get(ListOfItemsImages.KEY_DESC)));
	     holder.number.setText("" + (position+1));
	     
	     imageLoader.DisplayImage(dat.get(ListOfItemsImages.KEY_THUMBPATH), holder.thumbView);
	     
	     return vi;
	}
	
	class viewHolder
	 {
	   	TextView name;
	   	TextView code;
	   	TextView desc;
	   	TextView number;
	   	ImageView thumbView;
	   	ImageView tri;
	 }
}
