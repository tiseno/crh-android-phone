package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddJobsAdapter extends BaseAdapter {
	private static LayoutInflater inflater = null;
	private final Activity activity;
	private String index;
	private String[] colors;
	private ArrayList<Product> data;
	private AddJobsAdapter adapter;
	private HashMap<String, String> productData;
	private Global global;

	public AddJobsAdapter(Activity activity, ArrayList<Product> data,
			HashMap<String, String> productData) {
		this.activity = activity;
		this.global = new Global(activity);
		this.productData = productData;
		this.data = data;
		this.adapter = this;
		colors = activity.getResources().getStringArray(R.array.catColor);
		inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int clickingPos = position;
		View vi = convertView;
		viewHolder holder;

		if (convertView == null) {
			holder = new viewHolder();

			vi = inflater.inflate(R.layout.cell_job, null);
			vi.setTag(holder);
		} else {
			holder = (viewHolder) convertView.getTag();
		}

		holder.title = (TextView) vi.findViewById(R.id.cell_job_tv_title);
		holder.title.setText(data.get(position).JobTitle);
		holder.desc = (TextView) vi.findViewById(R.id.cell_job_tv_desc);

		if (data.get(position).Quantity != 0) {
			int quantity = (Integer) data.get(position).Quantity;
			if (quantity > 1)
				holder.desc.setText("Has " + data.get(position).Quantity
						+ " of those");
			else
				holder.desc.setText("Has " + data.get(position).Quantity
						+ " of these");
		} else {
			holder.desc.setText("Has none of these");

		}

		holder.btMinus = (Button) vi.findViewById(R.id.cell_job_bt_minus);
		holder.btMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (data.get(clickingPos).minusQuantity()) {
					updateOrInsertProduct(clickingPos);
				} else {
					removeProduct(clickingPos);
				}
				adapter.notifyDataSetChanged();
			}
		});

		holder.btPlus = (Button) vi.findViewById(R.id.cell_job_bt_plus);
		holder.btPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				data.get(clickingPos).plusQuantity();
				updateOrInsertProduct(clickingPos);
				adapter.notifyDataSetChanged();
			}
		});

		holder.btQuantity = (Button) vi.findViewById(R.id.cell_job_bt_qty);
		holder.btQuantity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popDialog(Global.QUANTITY, clickingPos);
			}
		});

		return vi;
	}

	private void removeProduct(int clickingPos) {
		Log.i("ProductID: ", "" + data.get(clickingPos).ProductID);
		if (data.get(clickingPos).ProductID == 0) {

		} else {
			global.da.DeleteProduct(data.get(clickingPos).ProductID);
		}

	}

	private void updateOrInsertProduct(int position) {
		ContentValues cv = new ContentValues();
		cv.put("Title", productData.get("Title"));
		cv.put("Code", productData.get("Code"));
		cv.put("Trade", productData.get("Trade"));
		cv.put("Quantity", data.get(position).Quantity);
		cv.put("Jobid", data.get(position).MyjobID);
		cv.put("Retail", productData.get("Retail"));
		cv.put("ItemID", productData.get("ItemID"));
		cv.put("Description", productData.get("Description"));
		cv.put("ImagePathS", productData.get("ImagePathS"));
		cv.put("ImagePathM", productData.get("ImagePathM"));
		cv.put("ImageGallery",  productData.get("ImageGallery"));
		cv.put("ImageTechnical",  productData.get("ImageTechnical"));
		
		if (data.get(position).ProductID == 0)
			data.get(position).ProductID = (int) global.da.insertProduct(cv);
		else {
			cv.put("ProductID", data.get(position).ProductID);
			global.da.insertProduct(cv);
		}
	}

	public void popDialog(final int action, final int clickingPos) {
		// custom dialog
		final Dialog dialog = new Dialog(activity,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.dialog_popup);
		dialog.setCancelable(true);

		TextView text = (TextView) dialog
				.findViewById(R.id.dialog_popup_tv_title);
		text.setText("Enter Quantity");

		final EditText field = (EditText) dialog
				.findViewById(R.id.dialog_popup_ed_textField);

		field.setInputType(InputType.TYPE_CLASS_NUMBER);

		Button btAdd = (Button) dialog.findViewById(R.id.dialog_popup_bt_Add);
		btAdd.setText("Set");
		btAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String quantity = field.getText().toString();
				if (!quantity.equals("")) {
					data.get(clickingPos).setQuantity(quantity);
					updateOrInsertProduct(clickingPos);
					adapter.notifyDataSetChanged();
					dialog.dismiss();
					global.hideKeyboard(v);

				} else {
					Toast.makeText(activity, "Quantity cannot empty !! ", 1000)
							.show();
				}
			}
		});

		Button btCancel = (Button) dialog
				.findViewById(R.id.dialog_popup_bt_cancel);
		btCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				field.clearFocus();
				global.hideKeyboard(v);
				dialog.dismiss();
				
			}
		});
		
		dialog.show();
		global.showKeyboard();
	}

	class viewHolder {
		TextView title;
		TextView desc;
		Button btMinus;
		Button btPlus;
		Button btQuantity;
	}
}
