package com.crh.novafusion;

public class CategoryItems 
{
	int id, application_id, parent;
	String name;
	
	public CategoryItems()
	{
		
	}
	
	public int getID()
	{
		return this.id;
	}
	
	public void setID(int id)
	{
		this.id = id;
	}
	
	public int getApplicationID()
	{
		return this.application_id;
	}
	
	public void setApplicationID(int application_id)
	{
		this.application_id = application_id;
	}
	
	public int getParentID()
	{
		return this.parent;
	}
	
	public void setParentID(int parent)
	{
		this.parent = parent;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
}
