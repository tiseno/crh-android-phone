package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CategoryAdapter extends BaseAdapter implements Filterable
{
	private static LayoutInflater inflater = null;
	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private String index, catName;
	private String[] colors;
	ArrayList<HashMap<String, String>> mOriginalValues;
	
	public CategoryAdapter(Activity activity, ArrayList<HashMap<String, String>> data, String index, String catName)
	{
		this.activity = activity;
		
		this.data = data;
		
		this.index = index;
		
		this.catName = catName;
		
		inflater = (LayoutInflater)this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		colors = activity.getResources().getStringArray(R.array.catColor);
	}
	
	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public HashMap<String, String> getItem(int position) 
	{
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		 final int clickingPos = position;
		 
		 View vi = convertView;
	     viewHolder holder;
	        
	     if(convertView == null)
	     {
	       	holder = new viewHolder();
	        	
	        vi = inflater.inflate(R.layout.category_list_row, null);
            vi.setTag(holder);	        
          }
	      else
	      {
	       	holder = (viewHolder)convertView.getTag();
	      }
	     
	     holder.name = (TextView)vi.findViewById(R.id.textView_name_cat);
	     
	     holder.name.setOnClickListener(new OnClickListener()
	     {
			@Override
			public void onClick(View arg0) 
			{
				 Intent intent = new Intent(activity, ListOfItems.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 
				 HashMap<String, String> dat = new HashMap<String, String>();
			     dat = data.get(clickingPos);
			     
				 Bundle bundle = new Bundle();
				 bundle.putString("catID", dat.get(ListOfCategoryItems.KEY_ID));
				 bundle.putString("colorIndex", index);
				 bundle.putString("catName", catName);
				 
				 intent.putExtras(bundle);
				 
				 activity.startActivity(intent);
				 activity.overridePendingTransition(R.anim.right_enter,
							R.anim.right_exit);
			}
	     });
	     
	     holder.showImages = (Button)vi.findViewById(R.id.btn_cat_images);
	    
	     
	     holder.showImages.setOnClickListener(new OnClickListener()
	     {
			@Override
			public void onClick(View arg0) 
			{
				 Intent intent = new Intent(activity, ListOfItemsImages.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 
				 HashMap<String, String> dat = new HashMap<String, String>();
			     dat = data.get(clickingPos);
				 
				 Bundle bundle = new Bundle();
				 bundle.putString("catID", dat.get(ListOfCategoryItems.KEY_ID));
				 bundle.putString("colorIndex", index);
				 bundle.putString("catName", catName);
					
				 intent.putExtras(bundle);
				 //
				 Log.i("catID: ", dat.get(ListOfCategoryItems.KEY_ID));
				 //
				 activity.startActivity(intent);
				 activity.overridePendingTransition(R.anim.right_enter,
							R.anim.right_exit);
			}
	     });
	     
	     holder.listBackground = (RelativeLayout) vi.findViewById(R.id.cell_category_menu_rl);
	     holder.listBackground.setBackgroundColor(Color.parseColor(colors[Integer.parseInt(index)]));
	     
	     HashMap<String, String> dat = new HashMap<String, String>();
	     dat = data.get(position);
	     
	     holder.name.setText(dat.get(ListOfCategoryItems.KEY_NAME));
	     
		return vi;
	}
	
	 class viewHolder
	 {
	   	TextView name;
	   	Button showImages;
	   	RelativeLayout listBackground;
	 }

	@Override
	public Filter getFilter() 
	{
		// TODO Auto-generated method stub

		Filter filter = new Filter() 
		{
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) 
            {
                data = (ArrayList<HashMap<String, String>>) results.values;
                
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) 
            {
                FilterResults results = new FilterResults();  // Holds the results of a filtering operation in values

                ArrayList<HashMap<String, String>> FilteredArrList = new ArrayList<HashMap<String, String>>();
                
                if (mOriginalValues == null) 
                {
                    // saves the original data in mOriginalValues
                    mOriginalValues = new ArrayList<HashMap<String, String>>(data);
                }

                /********
                 * 
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)  
                 *
                 ********/
                
                if (constraint == null || constraint.length() == 0) 
                {
                    // set the Original result to return  
                    results.count = mOriginalValues.size();
                    
                    results.values = mOriginalValues;
                } 
                else 
                {
                    constraint = constraint.toString().toLowerCase();
                    
                    for (int i = 0; i < mOriginalValues.size(); i++) 
                    {
                    	HashMap<String, String> dat = new HashMap<String, String>();
                    	dat = mOriginalValues.get(i);
                        
                    	String data = dat.get(ListOfCategoryItems.KEY_NAME);
                        
                        if(data.toLowerCase().contains(constraint.toString()))
                    	{
                        	FilteredArrList.add(dat);
                        }
                    }
                    
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
}
