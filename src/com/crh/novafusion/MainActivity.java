package com.crh.novafusion;

import java.util.HashMap;

import uk.co.senab.photoview.PhotoView;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;
import com.crh.novafusion.imageloader.ImageLoader;
import com.zxing.activity.CaptureActivity;

public class MainActivity extends Activity implements
		AsyncTaskCompleteListener<Object> {
	private WebServices ws;
	private ListView list;
	private MainAdapter adapter;
	private ImageButton openURL, btnIconSearch;
	private Button btScanBarcode;
	static final String KEY_TITLE = "name",
			KEY_DESC = "content.teaser_description", KEY_ID = "id";
	private Global global;
	private Button btMyjobs;
	private Button btBookmarks;
	private Button btnMore;
	private ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		global = new Global(this);
		Cursor c = global.da.getAllJob();

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		btnMore = (Button) findViewById(R.id.btn_more);
		btnMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,
						HelpActivity.class);

				startActivityForResult(intent, 0);
				overridePendingTransition(R.anim.slide_up,
						R.anim.no_anim);
			}
		});

		btScanBarcode = (Button) findViewById(R.id.btn_barcode);

		btScanBarcode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent openCameraIntent = new Intent(MainActivity.this,
						CaptureActivity.class);

				startActivityForResult(openCameraIntent, 0);
				overridePendingTransition(R.anim.slide_up,
						R.anim.no_anim);
			}
		});

		btMyjobs = (Button) findViewById(R.id.main_bt_myjob);
		btMyjobs.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,
						MyjobActivity.class);
				startActivityForResult(intent, 0);
				overridePendingTransition(R.anim.slide_up,
						R.anim.no_anim);
			}
		});

		btBookmarks = (Button) findViewById(R.id.main_bt_bookmarks);
		btBookmarks.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,
						BookmarksActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_up,
						R.anim.no_anim);
			}
		});

		openURL = (ImageButton) findViewById(R.id.imageButton1);

		openURL.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = "http://www.crh.com.au";

				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));

				startActivity(i);
				finish();
			}
		});

		btnIconSearch = (ImageButton) findViewById(R.id.btn_icon_search);

		btnIconSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// show the pop up button here
				popDialog();
			}
		});

		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		Class[] parameterTypes = new Class[1];
		parameterTypes[0] = String.class;

		ws = new WebServices();

		try {
			LoadingTask loadingTask = new LoadingTask(this, ws,
					WebServices.class.getMethod(
							"selectAllFromApplicationTableExcepParam", null),
					null, this);
			progressBar.setVisibility(View.VISIBLE);
			loadingTask.execute();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);

		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			// String scanResult = bundle.getString("result");

			Bundle bundle = data.getExtras();
			bundle.putString("mode", "barcode");

			Intent intent = new Intent(MainActivity.this,
					SearchByKeyWordsList.class);

			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_up,
					R.anim.no_anim);
			// Toast.makeText(this, "" + scanResult, 1000).show();
		}
	}

	@Override
	public void onTaskComplete(Object result) {
		progressBar.setVisibility(View.GONE);

		list = (ListView) findViewById(R.id.list);

		try {
			WebServices data = (WebServices) result;

			adapter = new MainAdapter(MainActivity.this, data.wsObject);

			list.setAdapter(adapter);

			// Click event for single list row
			list.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					@SuppressWarnings("unchecked")
					HashMap<String, String> item = (HashMap<String, String>) list
							.getAdapter().getItem(position);

					String catID = item.get(KEY_ID);
					String catName = item.get(KEY_TITLE);

					Intent intent = new Intent(MainActivity.this,
							ListOfCategoryItems.class);

					Bundle bundle = new Bundle();
					bundle.putString("catID", catID);
					bundle.putString("catName", catName);
					bundle.putString("colorIndex", "" + position);

					intent.putExtras(bundle);
					startActivity(intent);
					overridePendingTransition(R.anim.right_enter,
							R.anim.right_exit);
				}
			});

		} catch (ClassCastException e) {
			e.printStackTrace();
		}
	}

	private void popDialog() {
		// custom dialog
		final Dialog dialog = new Dialog(this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.dialog_popup);
		dialog.setCancelable(true);

		TextView text = (TextView) dialog
				.findViewById(R.id.dialog_popup_tv_title);
		text.setText("Search");

		final EditText field = (EditText) dialog
				.findViewById(R.id.dialog_popup_ed_textField);
		field.requestFocus();

		Button btnSearch = (Button) dialog
				.findViewById(R.id.dialog_popup_bt_Add);
		btnSearch.setText("Search");

		btnSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String strSearch = field.getText().toString();

				if (strSearch.length() > 0) {
					global.hideKeyboard(v);
					dialog.dismiss();

					Intent intent = new Intent(MainActivity.this,
							SearchByKeyWordsList.class);

					Bundle bundle = new Bundle();
					bundle.putString("keywords", strSearch);
					bundle.putString("mode", "keywords");

					intent.putExtras(bundle);
					startActivity(intent);
					overridePendingTransition(R.anim.slide_up,
							R.anim.no_anim);
				} else {
					Toast.makeText(MainActivity.this,
							"Enter keywords to start searching !!", 1000)
							.show();
				}
			}
		});

		Button btCancel = (Button) dialog
				.findViewById(R.id.dialog_popup_bt_cancel);

		btCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				field.clearFocus();
				global.hideKeyboard(v);
				dialog.dismiss();
			}
		});

		dialog.show();
		global.showKeyboard();
	}

	private void popDialog2() {
		// custom dialog
		final Dialog dialog = new Dialog(this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.dialog_image_gallery);
		dialog.setCancelable(true);

		String[] urls = {
				"http://www.iloveapps.hk/wp-content/uploads/2012/10/Perfect-Image.jpg",
				"http://www.team-bhp.com/forum/attachments/shifting-gears/1033178d1356977973-official-non-auto-image-thread-_mg_0143.jpg",
				"http://www.iloveapps.hk/wp-content/uploads/2012/10/Perfect-Image.jpg" };

		final ViewPager mViewPager = (ViewPager) dialog
				.findViewById(R.id.dialog_image_gallery_vPager);
		final SamplePagerAdapter adapter = new SamplePagerAdapter(this, urls);
		mViewPager.setAdapter(adapter);

		Button btClose = (Button) dialog
				.findViewById(R.id.dialog_image_gallery_bt_close);
		btClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		final Button btNext = (Button) dialog
				.findViewById(R.id.dialog_image_gallery_bt_next);

		final Button btPrev = (Button) dialog
				.findViewById(R.id.dialog_image_gallery_bt_prev);

		btNext.setTag("next");
		btPrev.setTag("prev");

		btNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
				setButtonVisible(btNext, mViewPager.getCurrentItem(),
						mViewPager.getAdapter().getCount() - 1);
				setButtonVisible(btPrev, mViewPager.getCurrentItem(),
						mViewPager.getAdapter().getCount() - 1);
			}
		});

		btPrev.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
				setButtonVisible(btPrev, mViewPager.getCurrentItem(),
						mViewPager.getAdapter().getCount() - 1);
				setButtonVisible(btNext, mViewPager.getCurrentItem(),
						mViewPager.getAdapter().getCount() - 1);
			}
		});

		dialog.show();

	}

	static void setButtonVisible(Button bt, int position, int itemCount) {
		if (bt.getTag().equals("next")) {
			if (position == itemCount)
				bt.setVisibility(View.GONE);
			else
				bt.setVisibility(View.VISIBLE);

		} else if (bt.getTag().equals("prev")) {
			if (position == 0)
				bt.setVisibility(View.GONE);
			else
				bt.setVisibility(View.VISIBLE);
		}
	}

	static class SamplePagerAdapter extends PagerAdapter {
		private ImageLoader imageLoader;
		private String[] urls;

		public SamplePagerAdapter(Context context, String[] urls) {
			imageLoader = new ImageLoader(context);
			this.urls = urls;
		}

		@Override
		public int getCount() {
			return urls.length;
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			PhotoView photoView = new PhotoView(container.getContext());
			imageLoader.DisplayImage(urls[position], photoView);
			// photoView.setImageResource(sDrawables[position]);

			// Now just add PhotoView to ViewPager and return it
			container.addView(photoView, LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);

			return photoView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

	}

}
