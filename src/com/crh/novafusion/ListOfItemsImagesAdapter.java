package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.crh.novafusion.imageloader.ImageLoader;

public class ListOfItemsImagesAdapter extends BaseAdapter
{
	private static LayoutInflater inflater = null;
	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private String index;
	private String[] colors;
	
	private ImageLoader imageLoader;
	
	public ListOfItemsImagesAdapter(Activity activity, ArrayList<HashMap<String, String>> data, String index)
	{
		this.activity = activity;
		
		this.data = data;
		
		this.index = index;
		
		inflater = (LayoutInflater)this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		colors = activity.getResources().getStringArray(R.array.catColor);
		
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}
	
	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View vi = convertView;
		viewHolder holder;
	        
	     if(convertView == null)
	     {
	    	holder = new viewHolder();
	        
	    	vi = inflater.inflate(R.layout.cat_images_list_column, null);
	    	vi.setTag(holder);	
         }
	     else
	     {
	    	 holder = (viewHolder)convertView.getTag();
	     }
	     
	     holder.title = (TextView)vi.findViewById(R.id.cat_images_list_title);
	     holder.thumbView = (ImageView)vi.findViewById(R.id.cat_images_list_img);
	     
	     HashMap<String, String> dat = new HashMap<String, String>();
	     dat = data.get(position);
	     
	     holder.title.setText(dat.get("code"));
	     
	     //getBitmapFromURL(dat.get(ListOfItemsImages.KEY_THUMBPATH));
	     //holder.thumbView.setImageBitmap(myBitmap);
	     
	     imageLoader.DisplayImage(dat.get(ListOfItemsImages.KEY_THUMBPATH), holder.thumbView);

		return vi;
	}

	 /*public void getBitmapFromURL(final String src) 
	 {
		 Thread networkThread = new Thread() 
		 {
			@Override
			public void run() 
			{
				try 
				{
					try 
					{
				        URL url = new URL(src);
				        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				        connection.setDoInput(true);
				        connection.connect();
				        InputStream input = connection.getInputStream();
				        
				        myBitmap = BitmapFactory.decodeStream(input);
				        
				        Log.e("Bitmap","returned");
				        
				        //return myBitmap;
					} 
					catch (IOException e) 
					{
				        e.printStackTrace();
				        Log.e("Exception",e.getMessage());
				        //return null;
					}
				}
				catch(Exception ex)
				{
					System.out.println("error ... " + ex.toString());
				}
				}
			};
				networkThread.start();
				
				try{networkThread.join();}catch(Exception e){}
	 }*/
	
	class viewHolder
	 {
	   	TextView title;
	   	ImageView thumbView;
	   	//Button showImages;
	   	//RelativeLayout listBackground;
	 }
}
