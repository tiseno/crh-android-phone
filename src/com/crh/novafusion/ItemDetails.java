package com.crh.novafusion;

public class ItemDetails 
{
	int id, application_id;
	String type, name, description, price, imagePath1, imagePath2, inc_gst, code, thumbPath, trade;
	
	public ItemDetails()
	{
		
	}
	
	public int getID()
	{
		return this.id;
	}
	
	public void setID(int id)
	{
		this.id = id;
	}
	
	public int getApplicationID()
	{
		return this.application_id;
	}
	
	public void setApplicationID(int application_id)
	{
		this.application_id = application_id;
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public String getPrice()
	{
		return this.price;
	}
	
	public void setPrice(String price)
	{
		this.price = price;
	}
	
	public String getImagePath1()
	{
		return this.imagePath1;
	}
	
	public void setImagePath1(String imagePath1)
	{
		this.imagePath1 = imagePath1;
	}

	public String getImagePath2()
	{
		return this.imagePath2;
	}
	
	public void setImagePath2(String imagePath2)
	{
		this.imagePath2 = imagePath2;
	}
	
	public String getIncGst()
	{
		return this.inc_gst;
	}
	
	public void setIncGst(String inc_gst)
	{
		this.inc_gst = inc_gst;
	}
	
	public String getCode()
	{
		return this.code;
	}
	
	public void setCode(String code)
	{
		this.code = code;
	}
	
	public String getThumbPath()
	{
		return this.thumbPath;
	}
	
	public void setThumbPath(String thumbPath)
	{
		this.thumbPath = thumbPath; 
	}
	
	public String getTrade()
	{
		return this.trade;
	}
	
	public void setTrade(String trade)
	{
		this.trade = trade;
	}
}
