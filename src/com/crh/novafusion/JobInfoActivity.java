package com.crh.novafusion;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;

public class JobInfoActivity extends Activity implements OnItemClickListener,
		OnClickListener {
	private ListView listview;
	private TextView title;
	private EditText etTitle;
	private ProgressBar progressBar;
	private ArrayList<Product> data = new ArrayList<Product>();
	private Global global;

	private Button btEdit, btnMore;
	private int MyjobId;
	private String Title;

	private TextView tvRetail;
	private TextView tvTrade;
	float totalRetail = 0.00f;
	float totalTrade = 0.00f;
	private LoadingTask loading;

	private Boolean isDeleteMode = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jobinfo);
		global = new Global(this);

		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
			MyjobId = bundle.getInt("MyjobID");
			Title = bundle.getString("Title");
		}

		initView();
	}

	public ProgressBar getPreogressBar() {
		return this.progressBar;
	}
	
	private void initView() {
		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		listview = (ListView) findViewById(R.id.jobinfo_lv_listview);
		listview.setOnItemClickListener(this);

		btEdit = (Button) findViewById(R.id.jobinfo_bt_edit);
		btEdit.setOnClickListener(this);
		
		btnMore = (Button) findViewById(R.id.jobinfo_bt_more);
		btnMore.setOnClickListener(this);

		title = (TextView) findViewById(R.id.jobinfo_tv_title);
		title.setText(Title);

		etTitle = (EditText) findViewById(R.id.jobinfo_et_title);
		etTitle.setText(Title);

		tvRetail = (TextView) findViewById(R.id.jobinfo_tv_total_retail);
		tvTrade = (TextView) findViewById(R.id.jobinfo_tv_total_trade);

		data = new ArrayList<Product>();
		loading = new LoadingTask();
		loading.execute();
	}
	
	private ArrayList<Product> getDataFromDB(){
		totalTrade = 0.0f;
		totalRetail = 0.0f;
		Cursor c = global.da.getProductByJobid(MyjobId);
		data.clear();
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			Product product = new Product();
			product.MyjobID = c.getInt(c.getColumnIndex("Jobid"));
			product.ProductID = c.getInt(c.getColumnIndex("ProductID"));
			product.ItemID = c.getInt(c.getColumnIndex("ItemID"));
			product.Title = c.getString(c.getColumnIndex("Title"));
			product.JobTitle = Title;
			product.ImagePathS = c.getString(c.getColumnIndex("ImagePathS"));
			product.ImagePathM = c.getString(c.getColumnIndex("ImagePathM"));
			product.Code = c.getString(c.getColumnIndex("Code"));
			product.Description = c.getString(c.getColumnIndex("Description"));
			product.Quantity = c.getInt(c.getColumnIndex("Quantity"));
			product.Retail = c.getString(c.getColumnIndex("Retail"));
			product.Trade = c.getString(c.getColumnIndex("Trade"));
			product.ImageGalery = c.getString(c.getColumnIndex("ImageGallery"));
			product.ImageTechnical = c.getString(c.getColumnIndex("ImageTechnical"));

			try {
				float _retail = 0.00f;
				_retail = Float.parseFloat(product.Retail.replaceAll(
						"[^\\d\\.]", ""));
				totalRetail += _retail;
			} catch (Exception ex) {

			}

			try {
				float _trade = 0.00f;
				_trade = Float.parseFloat(product.Trade.replaceAll("[^\\d\\.]",
						""));
				totalTrade += _trade;
			} catch (Exception ex) {

			}
			data.add(product);
		}
		
		return data;
	}

	public void proccessListData() {
		DecimalFormat df = new DecimalFormat("#.##");
		tvRetail.setText("" + totalRetail);
		tvTrade.setText("" + totalTrade);
		tvRetail.setText("" + df.format(totalRetail));
		// tvTrade.setText("" + df.format(totalTrade));

		JobInfoAdapter adapter = new JobInfoAdapter(JobInfoActivity.this, data);
		adapter.setIsDeleteMode(isDeleteMode);
		listview.setAdapter(adapter);
		resizeListView();
	}

	private void resizeListView() {
		JobInfoAdapter listAdapter = (JobInfoAdapter) listview.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(listview.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listview);
			listItem.setLayoutParams(new LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT));
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listview.getLayoutParams();
		params.height = totalHeight
				+ (listview.getDividerHeight() * (listAdapter.getCount() - 1));
		listview.setLayoutParams(params);
		listview.requestLayout();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

	}

	@Override
	protected void onResume() {
		proccessListData();
		super.onResume();
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
			case R.id.jobinfo_bt_edit:
				
				JobInfoAdapter adapter = (JobInfoAdapter) listview.getAdapter();
		
				if (isDeleteMode) 
				{
					String newTitle = etTitle.getText().toString();
			
					if (newTitle.equals("")) 
					{
						Toast.makeText(this, "Job Title cannot be empty!!", 1000)
							.show();
						return;
					} 
					else 
					{
						etTitle.setVisibility(View.GONE);
						title.setVisibility(View.VISIBLE);

						ContentValues cv = new ContentValues();
						cv.put("Title", newTitle + "");
						title.setText(newTitle + "");
						global.da.updateMyjob(cv, MyjobId);
		
						btEdit.setText("Edit");
						isDeleteMode = false;
					}
				}	 
				else 
				{
						etTitle.setVisibility(View.VISIBLE);
						title.setVisibility(View.INVISIBLE);
						btEdit.setText("Done");
						isDeleteMode = true;
				}

						adapter.setIsDeleteMode(isDeleteMode);
						listview.invalidateViews();
						
						break;
		
			case R.id.jobinfo_bt_more:
				
				// create a new bundle and insert type value
				Bundle bundle = new Bundle();
				bundle.putParcelableArrayList("products", data);
				bundle.putString("type", "products");
				bundle.putString("totalRetail", tvRetail.getText().toString());
				bundle.putString("totalTrade", tvTrade.getText().toString());
				
				//show the pop up button here
	        	global.popEmailDialog(JobInfoActivity.this, bundle);
				
				break;
		}
	}


	class LoadingTask extends AsyncTask<Integer, Integer, Object> {

		@Override
		protected void onPreExecute() {
			progressBar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected Object doInBackground(Integer... params) {
			
			return getDataFromDB();
		}

		@Override
		protected void onPostExecute(Object result) {
			proccessListData();
			progressBar.setVisibility(View.INVISIBLE);
			super.onPostExecute(result);
		}

	}

	@Override
	public void onBackPressed() {
		if (isDeleteMode) {
			BookmarksAdapter adapter = (BookmarksAdapter) listview.getAdapter();
			isDeleteMode = false;
			adapter.setIsDeleteMode(isDeleteMode);
			listview.invalidateViews();
			return;
		}
		super.onBackPressed();
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.left_enter, R.anim.left_exit);
	}
}
