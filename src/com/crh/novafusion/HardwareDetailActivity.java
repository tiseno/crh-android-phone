package com.crh.novafusion;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;
import com.crh.novafusion.imageloader.ImageLoader;

public class HardwareDetailActivity extends Activity implements
		AsyncTaskCompleteListener<Object>, OnClickListener {
	private ImageView image, ivViewImage, ivViewDraw;
	private RelativeLayout rl;
	private ImageView btBookmark;
	private TextView hardwareIndex, hardwareTitle;
	private TextView hardwareCode, hardwareTrade, hardwareRetail, hardwareDesc;
	private Button btAddJob, btnEnquiry, btnEmail;
	private Bundle bundle;
	private Global global;
	private LinearLayout ll;
	private TextView tvTitle;
	ContentValues cv;
	private String galleryKey, technicalKey;
	private WebServices ws;
	private ProgressBar progressBar;
	private int galleryOrTechnical = 0;

	// test data
	private static String[] urls = {
			"http://www.iloveapps.hk/wp-content/uploads/2012/10/Perfect-Image.jpg",
			"http://www.team-bhp.com/forum/attachments/shifting-gears/1033178d1356977973-official-non-auto-image-thread-_mg_0143.jpg",
			"http://www.iloveapps.hk/wp-content/uploads/2012/10/Perfect-Image.jpg" };

	//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		global = new Global(this);
		setContentView(R.layout.hardware_detail_activity);
		init();
	}

	private void init() {
		image = (ImageView) findViewById(R.id.hardware_detail_iv_hardware_image);
		ll = (LinearLayout) findViewById(R.id.linearView1);
		
		tvTitle = (TextView) findViewById(R.id.hardware_detail_tv_cat_name);
		
		ivViewImage = (ImageView) findViewById(R.id.hardware_detail_iv_hardware_showimage);
		ivViewImage.setOnClickListener(this);

		ivViewDraw = (ImageView) findViewById(R.id.hardware_detail_iv_hardware_showimage2);
		ivViewDraw.setOnClickListener(this);

		btBookmark = (ImageView) findViewById(R.id.hardware_detail_iv_hardware_addFav);
		btBookmark.setOnClickListener(this);

		hardwareIndex = (TextView) findViewById(R.id.hardware_detail_tv_hardware_index);
		hardwareTitle = (TextView) findViewById(R.id.hardware_detail_tv_hardware_title);

		hardwareCode = (TextView) findViewById(R.id.hardware_detail_tv_hardware_code);
		hardwareTrade = (TextView) findViewById(R.id.hardware_detail_tv_hardware_Trade);
		hardwareRetail = (TextView) findViewById(R.id.hardware_detail_tv_hardware_retail);
		hardwareDesc = (TextView) findViewById(R.id.hardware_detail_tv_hardware_desc);

		rl = (RelativeLayout) findViewById(R.id.hardware_detail_parentLayout);
		String[] colors = this.getResources().getStringArray(R.array.catColor);

		bundle = this.getIntent().getExtras();

		ws = new WebServices();
		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		if (bundle != null || bundle.containsKey("index")) {
			hardwareIndex
					.setText((Integer.parseInt(bundle.getString("index")) + 1)
							+ ") ");
			hardwareTitle.setText(bundle.getString("hardwareTitle"));

			hardwareCode.setText(bundle.getString("hardwareCode"));
			hardwareTrade.setText(bundle.getString("hardwareTrade"));
			hardwareRetail.setText(bundle.getString("hardwareRetail"));
			hardwareDesc
					.setText(Html.fromHtml(bundle.getString("hardwareDesc")));

			ImageLoader il = new ImageLoader(this);
			Log.i("imagePath ... ", bundle.getString("hardwareImagePath") + "");
			il.setImageSize(180);
			il.DisplayImage(bundle.getString("hardwareImagePath"), image);

			rl.setBackgroundColor(Color.parseColor(colors[Integer
					.parseInt(bundle.getString("colorIndex"))]));
			
			global.setTopbarColor(ll,
					Integer.parseInt(bundle.getString("colorIndex")));
			
			tvTitle.setText(bundle.getString("catName"));

			galleryKey = bundle.getString("hardwareGalleryImagePath") + "";
			technicalKey = bundle.getString("hardwareTechnicalImagePath") + "";

			cv = new ContentValues();
			cv.put("Title", bundle.getString("hardwareTitle"));
			cv.put("Code", bundle.getString("hardwareCode"));
			cv.put("Trade", bundle.getString("hardwareTrade"));
			cv.put("Retail", bundle.getString("hardwareRetail"));
			cv.put("Description", bundle.getString("hardwareDesc"));
			cv.put("ItemID", bundle.getString("hardwareID"));

			cv.put("ImagePathS", bundle.getString("hardwareImagePath"));
			cv.put("ImagePathM", bundle.getString("hardwareImagePath"));
			cv.put("ImageGallery", galleryKey);
			cv.put("ImageTechnical", technicalKey);
		}

		btAddJob = (Button) findViewById(R.id.hardware_detail_bt_add_job);
		btAddJob.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(HardwareDetailActivity.this,
						AddJobsActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_up, R.anim.no_anim);
			}
		});

		btnEnquiry = (Button) findViewById(R.id.hardware_detail_bt_add_equiry);
		btnEnquiry.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(HardwareDetailActivity.this,
						SendEnquiry.class);
				intent.putExtras(bundle);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_up, R.anim.no_anim);
			}
		});

		btnEmail = (Button) findViewById(R.id.hardware_detail_bt_add_email);
		btnEmail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// show the pop up button here
				bundle.putString("type", "none");

				global.popEmailDialog(HardwareDetailActivity.this, bundle);
			}
		});
	}

	@Override
	public void onTaskComplete(Object result) {
		progressBar.setVisibility(View.GONE);

		WebServices data = (WebServices) result;

		if (galleryOrTechnical == 1) {
			// if is gallery

			String[] urls = data.imagePath1_arr
					.toArray(new String[data.imagePath1_arr.size()]);

			if (urls.length > 0) {
				global.popImageGallery(urls, hardwareTitle.getText().toString());
			} else {
				Toast.makeText(this, "no gallery image", 1500).show();
			}
		} else {
			// if is technical

			String[] urls2 = data.imagePath2_arr
					.toArray(new String[data.imagePath2_arr.size()]);

			if (urls2.length > 0) {
				global.popImageGallery(urls2, hardwareTitle.getText()
						.toString());
			} else {
				Toast.makeText(this, "no technical image", 1500).show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.hardware_detail_iv_hardware_addFav:
			if (global.da.insertBookmark(cv)) {
				Toast.makeText(this, "Add to bookmark !!", 1000).show();
			}
			break;

		case R.id.hardware_detail_iv_hardware_showimage:

			galleryOrTechnical = 1;

			Class[] parameterTypes = new Class[1];
			parameterTypes[0] = String.class;

			Object[] parameters = new Object[1];
			parameters[0] = galleryKey;

			try {
				LoadingTask loadingTask = new LoadingTask(this, ws,
						WebServices.class.getMethod("selectGalleryImageByPath",
								parameterTypes), parameters, this);
				progressBar.setVisibility(View.VISIBLE);
				loadingTask.execute();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}

			break;

		case R.id.hardware_detail_iv_hardware_showimage2:

			galleryOrTechnical = 2;

			Class[] parameterTypes2 = new Class[1];
			parameterTypes2[0] = String.class;

			Object[] parameters2 = new Object[1];
			parameters2[0] = technicalKey;

			try {
				LoadingTask loadingTask = new LoadingTask(this, ws,
						WebServices.class.getMethod(
								"selectTechnicalImageByPath", parameterTypes2),
						parameters2, this);
				progressBar.setVisibility(View.VISIBLE);
				loadingTask.execute();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.left_enter, R.anim.left_exit);
	}
}
