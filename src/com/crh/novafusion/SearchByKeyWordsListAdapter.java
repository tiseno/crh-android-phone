package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;

import com.crh.novafusion.imageloader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SearchByKeyWordsListAdapter extends BaseAdapter
{
	private static LayoutInflater inflater = null;
	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private String index;
	private ImageLoader imageLoader;
	
	private int[] colors = new int[] { Color.parseColor("#F2F4F5"), Color.parseColor("#FFFFFF") }; 
	
	public SearchByKeyWordsListAdapter(Activity activity, ArrayList<HashMap<String, String>> data, String index)
	{
		this.activity = activity;
		
		this.data = data;
		
		this.index = index;
		
		inflater = (LayoutInflater)this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View vi = convertView;
		viewHolder holder;
		
		if(convertView == null)
	    {
	    	holder = new viewHolder();
	        
	    	vi = inflater.inflate(R.layout.search_items_list_row, null);
	    	vi.setTag(holder);	
	    }
	    else
	    {
	    	 holder = (viewHolder)convertView.getTag();
	    }
		
		int colorPos = position % colors.length;
		vi.setBackgroundColor(colors[colorPos]); 
		
		holder.name = (TextView)vi.findViewById(R.id.item_name_search);
		holder.code = (TextView)vi.findViewById(R.id.item_code_search);
		holder.retail = (TextView)vi.findViewById(R.id.item_retail_search);
		holder.thumbView = (ImageView)vi.findViewById(R.id.item_image_search);
		
		HashMap<String, String> dat = new HashMap<String, String>();
	    dat = data.get(position);
	     
	    holder.name.setText(dat.get(SearchByKeyWordsList.KEY_NAME));
	    holder.code.setText(dat.get(SearchByKeyWordsList.KEY_CODE));
		holder.retail.setText("$" + dat.get(SearchByKeyWordsList.KEY_PRICE));
		
		imageLoader.DisplayImage(dat.get(ListOfItemsImages.KEY_THUMBPATH), holder.thumbView);
	    
		return vi;
	}
	
	class viewHolder
	{
		TextView name;
		TextView code;
		TextView retail;
		ImageView thumbView;
	}
}
