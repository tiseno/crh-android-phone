package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainAdapter extends BaseAdapter
{
	private Context activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;
    private String[] colors;
    //public ImageLoader imageLoader; 
    
    public MainAdapter(Activity activity, ArrayList<HashMap<String, String>> data) 
    {
        this.activity = activity;
        
        this.data = data;
        
        inflater = (LayoutInflater)this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        colors = activity.getResources().getStringArray(R.array.catColor);
        
        //imageLoader=new ImageLoader(activity.getApplicationContext());
    }

	public int getCount() 
    {
        return data.size();
    }

    public HashMap<String, String> getItem(int position) 
    {
        return data.get(position);
    }

    public long getItemId(int position) 
    {
        return position;
    }
    
    public View getView(int position, View convertView, ViewGroup parent) 
    {
        View vi = convertView;
        viewHolder holder;
        
        if(convertView == null)
        {
        	holder = new viewHolder();
        	
            vi = inflater.inflate(R.layout.cell_main_menu, null);
            
            vi.setTag(holder);
        }
        else
        {
        	holder = (viewHolder)convertView.getTag();
        }

        holder.name = (TextView)vi.findViewById(R.id.textView_name);
        holder.desc = (TextView)vi.findViewById(R.id.textView_desc);
        
        holder.listBackground = (RelativeLayout) vi.findViewById(R.id.cell_main_menu_rl);
        
//        holder.listBackground.setBackgroundColor(Color.parseColor("#f3d839"));
        
        holder.listBackground.setBackgroundColor(Color.parseColor(colors[position]));
        	
        //TextView duration = (TextView)vi.findViewById(R.id.duration); // duration
        //ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); // thumb image
        
        HashMap<String, String> dat = new HashMap<String, String>();
        dat = data.get(position);
        
        // Setting all values in listview
        holder.name.setText(dat.get(MainActivity.KEY_TITLE));
        holder.desc.setText(dat.get(MainActivity.KEY_DESC));
        
        //duration.setText(song.get(CustomizedListView.KEY_DURATION));
        //imageLoader.DisplayImage(song.get(CustomizedListView.KEY_THUMB_URL), thumb_image);
        
        return vi;
    }
    
    class viewHolder
    {
    	TextView name;
    	TextView desc;
    	RelativeLayout listBackground;
    }
}
