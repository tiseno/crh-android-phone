package com.crh.novafusion.LoadingTask;

public interface AsyncTaskCompleteListener<T> {
	public void onTaskComplete(T result);
}
