package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;
import com.crh.novafusion.imageloader.ImageLoader;

public class BookmarksAdapter extends BaseAdapter implements
		AsyncTaskCompleteListener<Object> {
	private static LayoutInflater inflater = null;
	private final BookmarksActivity activity;
	private Global global;
	private ImageLoader imageLoader;
	private WebServices ws;
	private ArrayList<Product> data;
	private BookmarksAdapter adapter;
	private int selectPos;
	private Boolean isDeleteMode = false;

	public BookmarksAdapter(Activity activity, ArrayList<Product> data) {
		this.activity =(BookmarksActivity) activity;
		this.global = new Global(activity);
		this.data = data;
		this.adapter = this;
		imageLoader = new ImageLoader(this.activity);
		inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setIsDeleteMode(Boolean status) {
		this.isDeleteMode = status;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int clickingPos = position;
		View vi = convertView;
		viewHolder holder;

		if (convertView == null) {
			holder = new viewHolder();
			vi = inflater.inflate(R.layout.cell_bookmark, null);
			vi.setTag(holder);
		} else {
			holder = (viewHolder) convertView.getTag();
		}

		holder.title = (TextView) vi
				.findViewById(R.id.jobinfo_tv_product_title);
		holder.title.setText(data.get(position).Title);

		holder.code = (TextView) vi.findViewById(R.id.jobinfo_tv_product_code);
		holder.code.setText(data.get(position).Code);

		holder.image = (ImageView) vi.findViewById(R.id.jobinfo_iv_image);
		imageLoader.DisplayImage(data.get(position).ImagePathS, holder.image);
		imageLoader.setImageSize(80);

		holder.btView = (Button) vi.findViewById(R.id.jobinfo_bt_view);
		holder.btView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewItem(clickingPos);
			}
		});

		if (position == data.size() - 1) {
			View view = vi.findViewById(R.id.view);
			view.setVisibility(View.GONE);
		}

		holder.btDelete = (Button) vi.findViewById(R.id.jobinfo_bt_delete);
		if (isDeleteMode) {
			holder.btDelete.setVisibility(View.VISIBLE);
			holder.btDelete.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					removeItem(clickingPos);
				}
			});
		} else {
			holder.btDelete.setVisibility(View.INVISIBLE);
		}
		return vi;
	}

	private void removeItem(int clickingPos) {
		global.da.DeleteBookmark(data.get(clickingPos).ProductID);
		data.remove(clickingPos);
		this.notifyDataSetChanged();
	}

	private void viewItem(int clickingPos) {
		selectPos = clickingPos;
		ws = new WebServices();
		try {
			Class[] parameterTypes = new Class[1];
			parameterTypes[0] = int.class;

			Object[] parameters = new Object[1];
			parameters[0] = data.get(clickingPos).ItemID;

			LoadingTask loadingTask = new LoadingTask(activity, ws,
					WebServices.class.getMethod("getAppNameAndOrderByItemID",
							parameterTypes), parameters, this);

			activity.getPreogressBar().setVisibility(View.VISIBLE);

			loadingTask.execute();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

	}

	class viewHolder {
		TextView title;
		TextView code;
		ImageView image;
		Button btView;
		Button btDelete;
	}

	@Override
	public void onTaskComplete(Object result) {
		String colorIndex = "";

		for (int i = 0; i < ws.wsObject.size(); i++) {
			HashMap<String, String> dat = new HashMap<String, String>();
			dat = ws.wsObject.get(i);

			colorIndex = dat.get("ordering");
		}

		Intent intent = new Intent(activity, HardwareDetailActivity.class);

		Bundle bundle = new Bundle();
		bundle.putString("index", "0");
		bundle.putString("hardwareTitle", data.get(selectPos).Title);
		bundle.putString("hardwareCode", data.get(selectPos).Code);
		bundle.putString("hardwareTrade", data.get(selectPos).Trade);
		bundle.putString("hardwareRetail", data.get(selectPos).Retail);
		bundle.putString("colorIndex", colorIndex);
		bundle.putString("catName", "Bookmarks");
		bundle.putString("hardwareDesc", data.get(selectPos).Description);
		bundle.putString("hardwareImagePath", data.get(selectPos).ImagePathS);
		bundle.putString("hardwareThumbPath", data.get(selectPos).ImagePathS);
		bundle.putString("hardwareID", data.get(selectPos).ItemID + "");
		bundle.putString("hardwareGalleryImagePath", data.get(selectPos).ImageGalery);
		bundle.putString("hardwareTechnicalImagePath", data.get(selectPos).ImageTechnical);

		intent.putExtras(bundle);
		activity.getPreogressBar().setVisibility(View.GONE);
		activity.startActivity(intent);
		activity.overridePendingTransition(R.anim.right_enter,
				R.anim.right_exit);
	}
}
