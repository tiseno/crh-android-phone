package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class HelpActivity extends Activity implements OnClickListener {
	private ViewPager viewPager;
	private Button btAbout, btContact, btHelp;
	private Button btClose;
	private ImageView ivAbout, ivContact, ivHelp;
	private List<View> listViews;

	// contact page
	private final int FACEBOOK = 0, LINKEDIN = 1, TWITTER = 2;
	private Button btFacebook, btLinkedin, btTwitter;

	// help page
	String[] from = { "image", "desc" };
	int[] to = { R.id.cell_help_iv_image, R.id.cell_help_tv_desc };
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		initView();
	}

	private void initsecondView() {
		btFacebook = (Button) findViewById(R.id.view_help_screen2_bt_share2facebook);
		btFacebook.setTag(FACEBOOK);
		btFacebook.setOnClickListener(ShareListener);

		btLinkedin = (Button) findViewById(R.id.view_help_screen2_bt_share2linkedin);
		btLinkedin.setTag(LINKEDIN);
		btLinkedin.setOnClickListener(ShareListener);

		btTwitter = (Button) findViewById(R.id.view_help_screen2_bt_share2twitter);
		btTwitter.setTag(TWITTER);
		btTwitter.setOnClickListener(ShareListener);
	}

	private void initThirdView() {
		ListView list = (ListView) findViewById(R.id.view_help_screen3_listview);

		TypedArray imgs;
		imgs = this.getResources().obtainTypedArray(R.array.help_image);
		
		TypedArray descs;
		descs = this.getResources().obtainTypedArray(R.array.help_desc);
		
		for (int i = 0; i < imgs.length(); i++) {
			HashMap<String, Object> _data = new HashMap<String, Object>();
			_data.put("image", imgs.getResourceId(i, R.drawable.help_1));
			_data.put("desc", descs.getString(i));
			data.add(_data);
		}

		SimpleAdapter adapter = new SimpleAdapter(this, data, R.layout.cell_help, from, to);
		list.setAdapter(adapter);

	}

	private void initView() {
		btAbout = (Button) findViewById(R.id.activity_help_bt_about);
		btAbout.setTag(0);
		btAbout.setOnClickListener(this);
		ivAbout = (ImageView) findViewById(R.id.activity_help_im_cursor_about);

		btContact = (Button) findViewById(R.id.activity_help_bt_contact);
		btContact.setTag(1);
		btContact.setOnClickListener(this);
		ivContact = (ImageView) findViewById(R.id.activity_help_im_cursor_contact);

		btHelp = (Button) findViewById(R.id.activity_help_bt_help);
		btHelp.setTag(2);
		btHelp.setOnClickListener(this);
		ivHelp = (ImageView) findViewById(R.id.activity_help_im_cursor_help);

		btClose = (Button) findViewById(R.id.activity_help_bt_close);
		btClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		viewPager = (ViewPager) findViewById(R.id.activity_help_vf_viewPager);
		listViews = new ArrayList<View>();
		LayoutInflater mInflater = getLayoutInflater();
		listViews.add(mInflater.inflate(R.layout.view_help_screen1, null));
		listViews.add(mInflater.inflate(R.layout.view_help_screen2, null));
		listViews.add(mInflater.inflate(R.layout.view_help_screen3, null));
		viewPager.setAdapter(new SamplePagerAdapter(listViews));
		viewPager.setCurrentItem(0);
		viewPager.setOnPageChangeListener(new MyOnPageChangeListener());
	}

	private OnClickListener ShareListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			int key = (Integer) v.getTag();
			String uri = "";
			switch (key) {
			case FACEBOOK:
				uri = "https://www.facebook.com/crh.australia";
				break;

			case LINKEDIN:
				uri = "http://au.linkedin.com/pub/crh-australia/5a/b14/2a1";
				break;

			case TWITTER:
				uri = "https://twitter.com/crhaus";
				break;
			}

			Intent viewIntent = new Intent("android.intent.action.VIEW",
					Uri.parse(uri));
			startActivity(viewIntent);
		}

	};

	@Override
	public void onClick(View v) {
		int key = (Integer) v.getTag();
		viewPager.setCurrentItem(key);
	}

	@Override
	protected void onResume() {
		viewPager.setCurrentItem(viewPager.getCurrentItem());
		super.onResume();
	}

	static class SamplePagerAdapter extends PagerAdapter {
		public List<View> mListViews;

		public SamplePagerAdapter(List<View> mListViews) {
			this.mListViews = mListViews;
		}

		@Override
		public int getCount() {
			return mListViews.size();
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(mListViews.get(position), 0);
			return mListViews.get(position);
		}

		@Override
		public void destroyItem(View container, int position, Object obj) {
			((ViewPager) container).removeView(mListViews.get(position));
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

	}

	// page change listener
	public class MyOnPageChangeListener implements OnPageChangeListener {
		@Override
		public void onPageSelected(int arg0) {
			switch (arg0) {
			case 0:
				ivAbout.setVisibility(View.VISIBLE);
				ivContact.setVisibility(View.INVISIBLE);
				ivHelp.setVisibility(View.INVISIBLE);
				break;

			case 1:
				ivAbout.setVisibility(View.INVISIBLE);
				ivContact.setVisibility(View.VISIBLE);
				ivHelp.setVisibility(View.INVISIBLE);
				initsecondView();
				break;

			case 2:
				ivAbout.setVisibility(View.INVISIBLE);
				ivContact.setVisibility(View.INVISIBLE);
				ivHelp.setVisibility(View.VISIBLE);
				initThirdView();
				break;
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.no_anim,
				R.anim.slide_out_down);
	}
}
