package com.crh.novafusion;

import java.util.HashMap;

import com.crh.novafusion.LoadingTask.AsyncTaskCompleteListener;
import com.crh.novafusion.LoadingTask.LoadingTask;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ListOfItemsImages extends Activity implements
		AsyncTaskCompleteListener<Object> {
	private WebServices ws;
	private Bundle bundle;
	private String catID, colorIndex, catName;
	private GridView mainGridView;
	private ListOfItemsImagesAdapter adapter;
	private TextView txtName;
	private LinearLayout ll;
	private ProgressBar progressBar;

	static final String KEY_NAME = "name", KEY_TYPE = "type",
			KEY_APPID = "application_id", KEY_ID = "id", KEY_DESC = "desc",
			KEY_PRICE = "price", KEY_IMGPATH1 = "imagePath1",
			KEY_IMGPATH2 = "imagePath2", KEY_INCGST = "incgst",
			KEY_CODE = "code", KEY_THUMBPATH = "thumbPath",
			KEY_MIDDLE_IMAGE = "middleImage", KEY_TRADE = "trade",
			KEY_TRADE_SUFFIX = "tradeSuffix", KEY_PRICE_SUFFIX = "priceSuffix";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cat_images_list);

		mainGridView = (GridView) findViewById(R.id.gridView1);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		bundle = this.getIntent().getExtras();

		catID = bundle.getString("catID");
		colorIndex = bundle.getString("colorIndex");
		catName = bundle.getString("catName");

		txtName = (TextView) findViewById(R.id.imagesListName);
		txtName.setText(catName);

		ll = (LinearLayout) findViewById(R.id.linearView1);

		Global global = new Global(this);
		global.setTopbarColor(ll, Integer.parseInt(colorIndex));

		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		Class[] parameterTypes = new Class[1];
		parameterTypes[0] = int.class;

		Object[] parameters = new Object[1];
		parameters[0] = Integer.parseInt(catID);

		ws = new WebServices();

		try {
			LoadingTask loadingTask = new LoadingTask(this, ws,
					WebServices.class.getMethod(
							"selectAllFromCategoryItemTableByID",
							parameterTypes), parameters, this);

			progressBar.setVisibility(View.VISIBLE);

			loadingTask.execute();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		// list = (ListView) findViewById(R.id.list2);

		// list.setAdapter(null);

		super.onDestroy();
	}

	@Override
	public void onTaskComplete(Object result) {
		progressBar.setVisibility(View.GONE);

		try {
			WebServices data = (WebServices) result;

			adapter = new ListOfItemsImagesAdapter(this, data.wsObject,
					colorIndex);

			mainGridView.setAdapter(adapter);

			// Click event for single list row
			mainGridView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					@SuppressWarnings("unchecked")
					HashMap<String, String> item = (HashMap<String, String>) mainGridView
							.getAdapter().getItem(position);
					String itemName = item.get(KEY_NAME);
					String itemCode = item.get(KEY_CODE);
					String itemDesc = item.get(KEY_DESC);
					String itemMiddleImage = item.get(KEY_MIDDLE_IMAGE);
					String itemThumbPath = item.get(KEY_THUMBPATH);
					String itemID = item.get(KEY_ID);
					String itemTrade = "-";
					String itemImagePath1 = item.get(KEY_IMGPATH1);
					String itemImagePath2 = item.get(KEY_IMGPATH2);

					if (item.get(KEY_TRADE).length() > 0
							&& !item.get(KEY_TRADE).toString()
									.equals("Price on application")) {
						itemTrade = "$" + item.get(KEY_TRADE) + " "
								+ item.get(KEY_TRADE_SUFFIX);
					}

					String itemPrice = "-";

					if (item.get(KEY_PRICE).length() > 0
							&& !item.get(KEY_PRICE).toString()
									.equals("Price on application")) {
						itemPrice = "$" + item.get(KEY_PRICE) + " "
								+ item.get(KEY_PRICE_SUFFIX);
					}

					// Log.i("imagePath", itemMiddleImage);

					Intent intent = new Intent(ListOfItemsImages.this,
							HardwareDetailActivity.class);

					Bundle bundle = new Bundle();
					bundle.putString("index", position + "");
					bundle.putString("hardwareTitle", itemName);
					bundle.putString("hardwareCode", itemCode);
					bundle.putString("hardwareTrade", itemTrade);
					bundle.putString("hardwareRetail", itemPrice);
					bundle.putString("colorIndex", colorIndex);
					bundle.putString("hardwareDesc", itemDesc);
					bundle.putString("hardwareImagePath", itemMiddleImage);
					bundle.putString("hardwareThumbPath", itemThumbPath);
					bundle.putString("hardwareID", itemID);
					bundle.putString("hardwareGalleryImagePath", itemImagePath1);
					bundle.putString("hardwareTechnicalImagePath",
							itemImagePath2);

					intent.putExtras(bundle);

					startActivity(intent);
					overridePendingTransition(R.anim.right_enter,
							R.anim.right_exit);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.left_enter, R.anim.left_exit);
	}
}
