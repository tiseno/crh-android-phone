package com.crh.novafusion;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crh.novafusion.imageloader.ImageLoader;

public class AddJobsActivity extends Activity implements OnClickListener {
	private ListView jobList;
	private ImageView image;
	private Button btAddjob, btDone;
	private TextView hardwareTitle, hardwareDesc;
	private RelativeLayout rl;
	private LinearLayout ll;
	private TextView txtName;
	private Global global;

	private ArrayList<Product> data;
	private AddJobsAdapter adapter;
	private HashMap<String, String> productData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_addjob);
		init();
	}

	private void init() {
		rl = (RelativeLayout) findViewById(R.id.addjob_ly_parentLayout);
		ll = (LinearLayout) findViewById(R.id.add_job_topbar);
		String[] colors = this.getResources().getStringArray(R.array.catColor);

		hardwareTitle = (TextView) findViewById(R.id.addjob_tv_title);
		hardwareDesc = (TextView) findViewById(R.id.addjob_tv_desc);
		txtName = (TextView) findViewById(R.id.item_list_cat_name);

		image = (ImageView) findViewById(R.id.addjob_iv_image);

		Bundle bundle = this.getIntent().getExtras();

		if (bundle != null) {
			hardwareTitle.setText(bundle.getString("hardwareTitle"));

			hardwareDesc.setText(bundle.getString("hardwareCode"));

			ImageLoader il = new ImageLoader(this);
			il.DisplayImage(bundle.getString("hardwareImagePath"), image);

			rl.setBackgroundColor(Color.parseColor(colors[Integer
					.parseInt(bundle.getString("colorIndex"))]));
			
			txtName.setText(bundle.getString("catName"));

			global = new Global(this);
			global.setTopbarColor(ll,
					Integer.parseInt((bundle.getString("colorIndex"))));

			productData = new HashMap<String, String>();
			productData.put("Title", bundle.getString("hardwareTitle"));
			productData.put("Code", bundle.getString("hardwareCode"));
			productData.put("Trade", bundle.getString("hardwareTrade"));
			productData.put("ItemID", bundle.getString("hardwareID"));
			productData.put("Jobid", "");
			productData.put("Description", bundle.getString("hardwareDesc"));
			productData.put("Retail", bundle.getString("hardwareRetail"));
			productData.put("ImagePathS",bundle.getString("hardwareImagePath"));
			productData.put("ImagePathM", bundle.getString("hardwareImagePath"));
			productData.put("ImageGallery", bundle.getString("hardwareGalleryImagePath"));
			productData.put("ImageTechnical", bundle.getString("hardwareTechnicalImagePath"));
			
			//pass
			System.out.println("image gallery ... " + bundle.getString("hardwareGalleryImagePath"));
			System.out.println("image technical ... " + bundle.getString("hardwareTechnicalImagePath"));
		}

		btAddjob = (Button) findViewById(R.id.addjob_bt_new_job);
		btAddjob.setOnClickListener(this);

		btDone = (Button) findViewById(R.id.addjob_bt_done);
		btDone.setOnClickListener(this);

		jobList = (ListView) findViewById(R.id.addjob_lv_joblist);

		getJobsData(bundle.getString("hardwareCode"));
	}

	private void getJobsData(String code) {
		data = new ArrayList<Product>();

		Cursor c = global.da.getProductByCode(code);
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			Product product = new Product();
			product.MyjobID = c.getInt(c.getColumnIndex("MyjobID"));
			product.ProductID = c.getInt(c.getColumnIndex("ProductID"));
			product.Title = c.getString(c.getColumnIndex("Title"));
			product.JobTitle = c.getString(c.getColumnIndex("JobTitle"));
			product.setQuantity(c.getInt(c.getColumnIndex("Quantity")));
			data.add(product);
		}

		adapter = new AddJobsAdapter(this, data,productData);
		jobList.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addjob_bt_done:
			finish();
			break;

		case R.id.addjob_bt_new_job:
			popDialog();
			break;
		}
	}

	private void popDialog() {
		// custom dialog
		final Dialog dialog = new Dialog(this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.dialog_popup);
		dialog.setCancelable(true);

		TextView text = (TextView) dialog
				.findViewById(R.id.dialog_popup_tv_title);
		text.setText("Job Title");

		final EditText field = (EditText) dialog
				.findViewById(R.id.dialog_popup_ed_textField);

		Button btAdd = (Button) dialog.findViewById(R.id.dialog_popup_bt_Add);
		btAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String title = field.getText().toString();
				if (!title.equals("")) {
					addJob(title);
					dialog.dismiss();
					global.hideKeyboard(v);
				} else {
					Toast.makeText(AddJobsActivity.this,
							"Tittle cannot empty !! ", 1000).show();
				}
			}
		});

		Button btCancel = (Button) dialog
				.findViewById(R.id.dialog_popup_bt_cancel);
		btCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				field.clearFocus();
				global.hideKeyboard(v);
				dialog.dismiss();
				
			}
		});

		dialog.show();
		global.showKeyboard();
	}

	private void addJob(String title) {
		ContentValues cv = new ContentValues();
		cv.put("Title", "" + title);
		long id = global.da.insertMyjob(cv);
		Product product = new Product();
		product.MyjobID = (int) id;
		product.JobTitle = title;
		product.ProductID = 0;
		product.Title = "";

		data.add(product);
		adapter.notifyDataSetChanged();
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.no_anim,
				R.anim.slide_out_down);
	}
}
